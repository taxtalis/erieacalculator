from classes.DiceRoll import DiceRoll_1D6
from classes.Trial import MultiTrial

def calculateAverageDamage(weapons):
    chances = MultiTrial(weapons).chances()
    damage = 0
    for w in weapons:
        _, avg, _ = w.damageStats()
        damage = damage + avg
    damage = damage / len(weapons)
    bonus = DiceRoll_1D6.average
    IPCostsForAttackCycle = sum([x.IPCostsForAttackCycle.calculate() for x in weapons]) / len(weapons)
    return (damage + sum([chances[key] * key * bonus for key in chances if key != "Slip"])) / IPCostsForAttackCycle

def optimizeWeapons(character, allowMeele, allowRanged, allowShield):
    for w in character.equipment.weapons:
        w.setValue(0, True)
    
    if allowShield:
        for w in character.equipment.shields:
            w.setValue(0, True)
    
    bestDamage = 0
    bestWeapons = []
    for weaponMH in character.equipment.weaponsMH:
        if ((weaponMH.reach <= 2 and allowMeele or weaponMH.reach > 2 and allowRanged) 
            and weaponMH.setValue(1, False)):
            damage = calculateAverageDamage([weaponMH])
            
            if bestDamage < damage:
                bestDamage = damage
                bestWeapons.clear()
                bestWeapons.append(weaponMH)
            
            if not weaponMH.isTwohanded:
                for weaponOH in character.equipment.weaponsOH:
                    if ((weaponOH.reach <= 2 and allowMeele or weaponOH.reach > 2 and allowRanged)
                        and weaponOH.setValue(1, False)):
                        
                        damage = calculateAverageDamage([weaponMH, weaponOH])
                        if bestDamage < damage:
                            bestDamage = damage
                            bestWeapons.clear()
                            bestWeapons.append(weaponMH)
                            bestWeapons.append(weaponOH)
                        
                    weaponOH.setValue(0, False)
                if allowShield:
                    for shield in character.equipment.shields:
                        if(shield.setValue(1, False)):
                
                            damage = calculateAverageDamage([weaponMH])
                            if bestDamage < damage:
                                bestDamage = damage
                                bestWeapons.clear()
                                bestWeapons.append(weaponMH)
                                bestWeapons.append(shield)
                        
                        shield.setValue(0, False)
        weaponMH.setValue(0, False)
    
    for weapon in bestWeapons:
        weapon.setValue(1, True)

    character.equipment.uiWeapons.update()
        