from classes.ValueProperty import ValueProperty
from classes.UI import UIDamage
from classes.Costs import Costs

class Weapon(ValueProperty):
    def __init__(self, character, name, skills, isTwohanded, STRequirement, constraintDependencies, diceRoll, damageBonus = 0, reach = 1, IPCostsForAttackCycle = 5, tooltip = None):
        ValueProperty.__init__(self, "Equipment", "/".join([s.name for s in skills]), name, tooltip, name, 0, 1, 1)
        self.character = character
        self.skills = skills
        self.isTwohanded = isTwohanded
        self.STRequirement = STRequirement
        self.baseDamage = diceRoll
        self.damageBonus = damageBonus
        self.reach = reach
        self.tooltip = tooltip
        self.isOffhand = False
        self.UIDamage = UIDamage(self.damageStats, "Equipment", id(self), self.name, "Minimum, Durchschnitt, Maximum")
        self.copies = []
        self.IPCostsForAttackCycle = Costs()
        self.IPCostsForAttackCycle.add(lambda v, d: IPCostsForAttackCycle, None)
        
        # set ST update dependency
        character.ST.dependingProperties.add(self)
        
        # add constraint that only one item per hand can be active
        self.constraints.add(lambda v, d: v == 0 
                             or sum([w.getValue() for w in d if w.isOffhand == self.isOffhand 
                                     or self.isOffhand and w.isTwohanded 
                                     or self.isTwohanded and w.isOffhand]) == 0, constraintDependencies)
        
    def copy(self, constraintDependencies, isOffhand = True):
        IPCostsForAttackCycle = self.IPCostsForAttackCycle.costFunctions[0](0, None)
        w = Weapon(self.character, self.name, self.skills, self.isTwohanded, self.STRequirement, constraintDependencies, self.baseDamage, self.damageBonus, self.reach, IPCostsForAttackCycle, self.tooltip)
        w.isOffhand = isOffhand
        
        w.copies.append(self)
        self.copies.append(w)
        return w
    
    def getSkill(self):
        skill = self.skills[0]
        for s in self.skills[1:]:
            if s.getValue() > skill.getValue():
                skill = s
        return skill
    
    def update(self, updated=None):
        ValueProperty.update(self, updated)
        self.UIDamage.update(updated)
    
    def strengthAdjustment(self):
        adjustment = self.character.ST.getValue() - self.STRequirement
        if adjustment > 0:
            adjustment = int(adjustment / 2)
        return adjustment
        
    def damageStats(self):
        adjustment = self.strengthAdjustment() + self.damageBonus
        minimum = max(0, self.baseDamage.minimum + adjustment)
        average = self.baseDamage.average + adjustment
        maximum = self.baseDamage.maximum + adjustment
        return minimum, average, maximum
        
    def damage(self):  
        adjustment = self.strengthAdjustment() + self.damageBonus
        return self.baseDamage.roll() + adjustment
    
    def dualWieldMalus(self):
        dualWieldMalus = self.character.equipment.mainhandMalus.getValue()
        if self.isOffhand:
            dualWieldMalus = self.character.equipment.offhandMalus.getValue()
        return dualWieldMalus
        
    def chances(self, adjustment = 0):
        return self.getSkill().trial.chances(-self.dualWieldMalus() + self.trialAdjustments() + adjustment)
    
    def roll(self, adjustment = 0):
        LoS, slip, name = self.getSkill().trial.roll(-self.dualWieldMalus() + self.trialAdjustments() + adjustment)
        name = self.name
        prefix = "Haupthand"
        if self.isOffhand:
            prefix = "Nebenhand"
        return LoS, slip, prefix+": "+name
    
    def trialAdjustments(self):
        return self.character.equipment.shieldBonus.getValue()
    
class Bow(Weapon):
    def __init__(self, character, name, skills, isTwohanded, STRequirement, constraintDependencies, diceRoll, damageBonus = 0, reach = 1, IPCostsForAttackCycle = 5, tooltip = None):
        Weapon.__init__(self, character, name, skills, isTwohanded, STRequirement, constraintDependencies, diceRoll, damageBonus, reach, IPCostsForAttackCycle, tooltip)
        
    def trialAdjustments(self):
        return self.character.find("Talents", "Guter Schütze").getValue()

class Shield(ValueProperty):
    def __init__(self, name, encumbrance, shieldBonus, stability, coverRating, constraintDependencies):
        ValueProperty.__init__(self, "Equipment", "Schild", name, None, name, 0, 1, 1)
        self.encumbrance = encumbrance
        self.shieldBonus = shieldBonus
        self.stability = stability
        self.coverRating = coverRating
        self.isTwohanded = False
        self.isOffhand = True
        
        # add constraint that only one item per hand can be active
        self.constraints.add(lambda v, d: v == 0 
                             or sum([w.getValue() for w in d if w.isOffhand == self.isOffhand 
                                     or self.isOffhand and w.isTwohanded 
                                     or self.isTwohanded and w.isOffhand]) == 0, constraintDependencies)