from uuid import uuid4

class ValueProperty:
    def __init__(self, category, subcategory, name, tooltip, shortname, boundLower, boundUpper, uiSliderResolution):
        self.uuid = str(uuid4())
        self.name = name
        self.shortname = shortname
        self.boundLower = BoundLower(self, boundLower)
        self.boundUpper = BoundUpper(self, boundUpper)
        self.value = boundLower
        self.constraints = Constraints(self)
        self.costs = Costs()
        self.dependingProperties = set()
        self.UI = UI(self, category, uiSliderResolution, tooltip)
        
    def sideeffects(self, value):
        pass
        
    def setValue(self, value, updateUI):
        value = min(max(int(value), self.boundLower.getValue()), self.boundUpper.getValue())
        valid = False
        if self.constraints.check(value):
            self.sideeffects(value)
            self.value = value
            valid = True
        if updateUI:
            self.UI.update()
        return valid
    
    def getValue(self):
        return self.value
        
    def updateBounds(self, lower, upper):
        self.UI.updateRanges()
        self.setValue(self.value, True)
               
    def getCosts(self):
        return self.costs.calculate(self.value)
    
    def update(self, updated=None):
        self.UI.update(updated)
    
from classes.Bounds import BoundLower, BoundUpper
from classes.Costs import Costs
from classes.Constraints import Constraints
from classes.UI import UI