from helperFunctions import round_halfup
from classes.Function import Function

class Derivation:
    def __init__(self, valueProperty, derivationFunction, dependencies):
        self._valueProperty = valueProperty        
        self.baseFunction = Function(derivationFunction, dependencies)
        for d in dependencies:
            d.dependingProperties.add(self._valueProperty)
            
        self.bonusFunctions = []
        self.bonusFunctionsOptimizable = []
        self.malusFunctions = []
        self.malusFunctionsOptimizable = []
        
        self.baseMinimum = self.baseFunction.getMinimum()
        self.baseMaximum = self.baseFunction.getMaximum()

        self.bonusOptimizableMaximum = 0
        self.malusOptimizableMaximum = 0
        
    def addBonus(self, bonusFunction, dependencies, isOptimizable):
        function = Function(bonusFunction, dependencies)
        if isOptimizable:
            self.bonusFunctionsOptimizable.append(function)
            self.bonusOptimizableMaximum = self.bonusOptimizableMaximum + function.getMaximum()
        else:
            self.bonusFunctions.append(function)        
        for d in dependencies:
            d.dependingProperties.add(self._valueProperty)
    
    def addMalus(self, malusFunction, dependencies, isOptimizable):
        function = Function(malusFunction, dependencies)
        if isOptimizable:
            self.malusFunctionsOptimizable.append(function)
            self.malusOptimizableMaximum = self.malusOptimizableMaximum + function.getMaximum()
        else:
            self.malusFunctions.append(function)
        for d in dependencies:
            d.dependingProperties.add(self._valueProperty)
    
    def calculateBaseValue(self):
        return self.baseFunction.getValue()
    
    def calculateAdditionals(self, unoptimizable, optimizable):
        addition = 0
        if unoptimizable:
            addition = addition + sum([f.getValue() for f in self.bonusFunctions])
            addition = addition - sum([f.getValue() for f in self.malusFunctions])
            
        if optimizable:
            addition = addition + sum([f.getValue() for f in self.bonusFunctionsOptimizable])
            addition = addition - sum([f.getValue() for f in self.malusFunctionsOptimizable])
        return addition
    
    def calculate(self, rounding = True):
        value = self.calculateBaseValue() + self.calculateAdditionals(True, True)
        if rounding:
            return round_halfup(value)
        return value

    def getMinimum(self):
        return self.baseMinimum + self.calculateAdditionals(True, False) - self.malusOptimizableMaximum
    
    def getMaximum(self):
        return self.baseMaximum + self.calculateAdditionals(True, False) + self.bonusOptimizableMaximum
    
    def getBonusDependencies(self, unoptimizable, optimizable):
        dependencies = set()
        if unoptimizable:
            for b in self.bonusFunctions:
                    dependencies.update(b.arguments)
        if optimizable:
            for b in self.bonusFunctionsOptimizable:
                    dependencies.update(b.arguments)
        return dependencies
    
    def getMalusDependencies(self, unoptimizable, optimizable):
        dependencies = set()
        if unoptimizable:
            for b in self.malusFunctions:
                    dependencies.update(b.arguments)
        if optimizable:
            for b in self.malusFunctionsOptimizable:
                    dependencies.update(b.arguments)
        return dependencies
    
    def getBaseDependencies(self):
        return self.baseFunction.arguments