class Constraints:
    def __init__(self, valueProperty):
        self._valueProperty = valueProperty
        self._constraintFunctions = []
        self.dependencies = []
        
    def add(self, constraintFunction, dependencies):
        self._constraintFunctions.append(constraintFunction)
        self.dependencies.append(dependencies)
        
    def check(self, value):
        for index in range(len(self._constraintFunctions)):
            if not self._constraintFunctions[index](value, self.dependencies[index]):
                return False
        return True
