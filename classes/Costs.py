from helperFunctions import round_halfup

class Costs:
    def __init__(self):
        self.costFunctions = []
        self.dependencies = []
        
    def add(self, costFunction, dependencies):
        self.costFunctions.append(costFunction)
        self.dependencies.append(dependencies)
        
    def calculate(self, value = 0):
        costs = 0
        for index in range(len(self.costFunctions)):
            costs = costs + self.costFunctions[index](value, self.dependencies[index])
        
        return round_halfup(costs)