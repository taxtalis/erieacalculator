from classes.Trial import MultiTrial

from classes.CharacterProperty import DerivedPropertyBound, DerivedProperty, Attribute, Advantage, Disadvantage, Skill, Talent
from classes.Equipment import Equipment
import PySimpleGUI as psg
from math import ceil

# cost functions
def costFunctionAttributeReduction(value, dependency):
    return - ([0, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144])[value] * dependency.getValue()

def costFunctionSkillReduction(value, dependency):
    return - ([0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55])[value] * dependency.getValue()

# derivation functions

def derivationFunction1(X, Y):
    return (2 * X.getValue() + Y.getValue()) / 3

def derivationFunction2(X, Y):
    return X.getValue() + Y.getValue()

def derivationFunction3(X):
    return 15 + 5 * X.getValue()

def derivationFunction4(X, Y):
    return (2 * X.getValue() + Y.getValue()) / 2

# bound constraint functions, USE SET VALUE FOR Z NOT DERIVED VALUE!

def boundConstraintFunction1XL(Z, Y): # Z = round((2X + Y)/3)
    return ceil((3 * (Z.value - Z.derivation.bonusOptimizableMaximum - Z.derivation.calculateAdditionals(True, False)) - 1 - Y.getValue()) / 2)

def boundConstraintFunction1XU(Z, Y):
    return ceil((3 * Z.value - Z.derivation.calculateAdditionals(True, False) + Z.derivation.malusOptimizableMaximum + 1 - Y.getValue()) / 2)

def boundConstraintFunction1YL(Z, X):
    #print(Z.shortname, Z.value, X.shortname, X.value)
    return 3 * (Z.value - Z.derivation.bonusOptimizableMaximum - Z.derivation.calculateAdditionals(True, False)) - 1 - 2 * X.getValue()

def boundConstraintFunction1YU(Z, X):
    #print(Z.shortname, Z.value, X.shortname, X.value)
    return 3 * Z.value - Z.derivation.calculateAdditionals(True, False) + Z.derivation.malusOptimizableMaximum + 1 - 2 * X.getValue()

def boundConstraintFunction2L(Z, Y):
    return Z.value - Z.derivation.bonusOptimizableMaximum - Z.derivation.calculateAdditionals(True, False) - Y.getValue()

def boundConstraintFunction2U(Z, Y):
    return Z.value - Z.derivation.calculateAdditionals(True, False) + Z.derivation.malusOptimizableMaximum - Y.getValue()

def boundConstraintFunction3L(Z):
    return ceil((Z.value - Z.derivation.bonusOptimizableMaximum - Z.derivation.calculateAdditionals(True, False) -15)/5)

def boundConstraintFunction3U(Z):
    return ceil((Z.value - Z.derivation.calculateAdditionals(True, False) + Z.derivation.malusOptimizableMaximum -15)/5)

def boundConstraintFunction4XL(Z, Y):
    return ceil((2 * (Z.value - Z.derivation.bonusOptimizableMaximum - Z.derivation.calculateAdditionals(True, False)) - 1 - Y.getValue()) / 2)

def boundConstraintFunction4XU(Z, Y):
    return ceil((2 * Z.value - Z.derivation.calculateAdditionals(True, False) + Z.derivation.malusOptimizableMaximum + 1 - Y.getValue()) / 2)

def boundConstraintFunction4YL(Z, X):
    #print(Z.shortname, Z.value, X.shortname, X.value)
    return 2 * (Z.value - Z.derivation.bonusOptimizableMaximum - Z.derivation.calculateAdditionals(True, False)) - 1 - 2 * X.getValue()

def boundConstraintFunction4YU(Z, X):
    #print(Z.shortname, Z.value, X.shortname, X.value)
    return 2 * Z.value - Z.derivation.calculateAdditionals(True, False) + Z.derivation.malusOptimizableMaximum - 2 * X.getValue()

# bound constraints for when Ini must not fall below a certain value
def boundConstraintFunction2DerivedAssosiation(Z, X):
    return Z.boundLower.getValue() - Z.derivation.calculateAdditionals(True, False) - X.getValue()


# constraint functions

def constraintFunctionExclusive(value, dependencies):
    if value == 1:
        for s in dependencies:
            if s.getValue() == 1:
                return False
    return True


    

class Character:
    def __init__(self):      
        
        self.uiXP = {}
        
        self.properties = []
        self.propertiesDict = {}
        
        # attributes
        # name, shortname, costsFunction, costDependencies, tooltip
        ST = Attribute("Stärke", "ST", None)
        KO = Attribute("Konsitution", "KO", None)
        GE = Attribute("Geschicklichkeit", "GE", None)
        IN = Attribute("Intelligenz", "IN", None)
        WI = Attribute("Willenskraft", "WI", None)
        CH = Attribute("Charisma", "CH", None)
        MU = Attribute("Mut", "MU", None)
        WA = Attribute("Wahrnehmung", "WA", None)
        self.ST = ST
        self.KO = KO
        self.GE = GE
        self.IN = IN
        self.WI = WI
        self.CH = CH
        self.MU = MU
        self.WA = WA
        self.attributes = [ST, KO, GE, IN, WI, CH, MU, WA]
        self.properties.extend(self.attributes)
        self.propertiesDict["Attribute"] = self.attributes
        
        # derived properties
        # name, shortname, boundStep, derivationFunction, derivationDependencies, tooltip
        Ref = DerivedProperty("Reflex", "Ref", 1, derivationFunction1, [GE, MU], None)
        Res = DerivedProperty("Resilienz", "Res", 1, derivationFunction1, [IN, WI], None)
        Aus = DerivedProperty("Ausdauer", "Aus", 1, derivationFunction1, [KO, WI], None)
        Bel = DerivedProperty("Belastbarkeit", "Bel", 1, derivationFunction2, [ST, KO], None)
        Ini = DerivedProperty("Initiative", "Ini", 1, derivationFunction2, [MU, WA], None)
        LP  = DerivedProperty("Lebenspunkte", "LP", 5, derivationFunction3, [KO], None)
        FP  = DerivedProperty("Fokuspunkte", "FP", 1, derivationFunction4, [WI, WA], None)
        self.Ref = Ref
        self.Res = Res
        self.Aus = Aus
        self.Bel = Bel
        self.Ini = Ini
        self.LP = LP
        self.FP = FP
        self.derivedProperties = [Ref, Res, Aus, Bel, Ini, LP, FP]    
        self.properties.extend(self.derivedProperties)
        self.propertiesDict["Abgeleitete Eigenschaften"] = self.derivedProperties
        
        # attribute bound constraints from derived properties
        # Ref
        GE.boundLower.add(boundConstraintFunction1XL, [Ref, MU], True)
        GE.boundUpper.add(boundConstraintFunction1XU, [Ref, MU], True)
        MU.boundLower.add(boundConstraintFunction1YL, [Ref, GE], True)
        MU.boundUpper.add(boundConstraintFunction1YU, [Ref, GE], True)
        # Res
        IN.boundLower.add(boundConstraintFunction1XL, [Res, WI], True)
        IN.boundUpper.add(boundConstraintFunction1XU, [Res, WI], True)
        WI.boundLower.add(boundConstraintFunction1YL, [Res, IN], True)
        WI.boundUpper.add(boundConstraintFunction1YU, [Res, IN], True)
        # Aus
        KO.boundLower.add(boundConstraintFunction1XL, [Aus, WI], True)
        KO.boundUpper.add(boundConstraintFunction1XU, [Aus, WI], True)
        WI.boundLower.add(boundConstraintFunction1YL, [Aus, KO], True)
        WI.boundUpper.add(boundConstraintFunction1YU, [Aus, KO], True)
        # Bel
        ST.boundLower.add(boundConstraintFunction2L, [Bel, KO], True)
        ST.boundUpper.add(boundConstraintFunction2U, [Bel, KO], True)
        KO.boundLower.add(boundConstraintFunction2L, [Bel, ST], True)
        KO.boundUpper.add(boundConstraintFunction2U, [Bel, ST], True)
        # Ini
        MU.boundLower.add(boundConstraintFunction2L, [Ini, WA], True)
        MU.boundUpper.add(boundConstraintFunction2U, [Ini, WA], True)
        MU.boundLower.add(boundConstraintFunction2DerivedAssosiation, [Ini, WA], False)
        WA.boundLower.add(boundConstraintFunction2L, [Ini, MU], True)
        WA.boundUpper.add(boundConstraintFunction2U, [Ini, MU], True)
        WA.boundLower.add(boundConstraintFunction2DerivedAssosiation, [Ini, MU], False)
        
        # LP
        KO.boundLower.add(boundConstraintFunction3L, [LP], True)
        KO.boundUpper.add(boundConstraintFunction3U, [LP], True)
        # FP
        WI.boundLower.add(boundConstraintFunction4XL, [FP, WA], True)
        WI.boundUpper.add(boundConstraintFunction4XU, [FP, WA], True)
        WA.boundLower.add(boundConstraintFunction4YL, [FP, WI], True)
        WA.boundUpper.add(boundConstraintFunction4YU, [FP, WI], True)
        
        # advantages
        
        advantages = []
        # name, boundUpper, costsFunction, tooltip
        gAus = Advantage("Ausdauernd", 1, lambda v, d: v * 20, None)
        gIni = Advantage("Entscheidungsfreudig", 1, lambda v, d: v * 20, None)
        advantages.append(Advantage("Gefahreninstinkt", 1, lambda v, d: v * 20, None))
        gGST = Advantage("Gesegnet (Stärke)", 1, lambda v, d: v * 40, None)
        gGKO = Advantage("Gesegnet (Konsitution)", 1, lambda v, d: v * 40, None)
        gGGE = Advantage("Gesegnet (Geschicklichkeit)", 1, lambda v, d: v * 40, None)
        gGIN = Advantage("Gesegnet (Intelligenz)", 1, lambda v, d: v * 40, None)
        gGWI = Advantage("Gesegnet (Willenskraft)", 1, lambda v, d: v * 40, None)
        gGCH = Advantage("Gesegnet (Charisma)", 1, lambda v, d: v * 40, None)
        gGMU = Advantage("Gesegnet (Mut)", 1, lambda v, d: v * 40, None)
        gGWA = Advantage("Gesegnet (Wahrnehmung)", 1, lambda v, d: v * 40, None)
        gRef = Advantage("Gewandt", 1, lambda v, d: v * 20, None)
        advantages.append(Advantage("Schicksalsgunst", 1, lambda v, d: v * 20, None))
        gINF = Advantage("Gutes Gedächtnis", 1, lambda v, d: v * 30, None)
        advantages.append(Advantage("Tapfer", 1, lambda v, d: v * 10, None))
        gBel = Advantage("Lasttier", 1, lambda v, d: v * 20, None)
        advantages.append(Advantage("Scharfsinnig", 1, lambda v, d: v * 20, None))
        gRes = Advantage("Unbeugsam", 1, lambda v, d: v * 20, None)
        gLP = Advantage("Vital", 1, lambda v, d: v * 20, None)
        gFP = Advantage("Fokussiert", 1, lambda v, d: v * 20, None)
        self.gAus = gAus
        self.gIni = gIni
        self.gGST = gGST
        self.gGKO = gGKO
        self.gGGE = gGGE
        self.gGIN = gGIN
        self.gGWI = gGWI
        self.gGCH = gGCH
        self.gGMU = gGMU
        self.gGWA = gGWA
        self.gRef = gRef
        self.gINF = gINF
        self.gBel = gBel
        self.gRes = gRes
        self.gLP = gLP
        self.gFP = gFP
        
        self.advantagesDerivedProperties = [gAus, gIni, gRef, gINF, gBel, gRes, gLP, gFP]
        self.advantagesBlessed = [gGST, gGKO, gGGE, gGIN, gGWI, gGCH, gGMU, gGWA]
        
        self.advantages = advantages + self.advantagesDerivedProperties + self.advantagesBlessed
        self.properties.extend(self.advantages)
        self.propertiesDict["Gaben"] = self.advantages
        
        # advantages constraints
    
        gGST.constraints.add(constraintFunctionExclusive, [      gGKO, gGGE, gGIN, gGWI, gGCH, gGMU, gGWA])
        gGKO.constraints.add(constraintFunctionExclusive, [gGST,       gGGE, gGIN, gGWI, gGCH, gGMU, gGWA])
        gGGE.constraints.add(constraintFunctionExclusive, [gGST, gGKO,       gGIN, gGWI, gGCH, gGMU, gGWA])
        gGIN.constraints.add(constraintFunctionExclusive, [gGST, gGKO, gGGE,       gGWI, gGCH, gGMU, gGWA])
        gGWI.constraints.add(constraintFunctionExclusive, [gGST, gGKO, gGGE, gGIN,       gGCH, gGMU, gGWA])
        gGCH.constraints.add(constraintFunctionExclusive, [gGST, gGKO, gGGE, gGIN, gGWI,       gGMU, gGWA])
        gGMU.constraints.add(constraintFunctionExclusive, [gGST, gGKO, gGGE, gGIN, gGWI, gGCH,       gGWA])
        gGWA.constraints.add(constraintFunctionExclusive, [gGST, gGKO, gGGE, gGIN, gGWI, gGCH, gGMU      ])

        # advantages effects
        
        Ref.derivation.addBonus(lambda d: d.getValue(), [gRef], True)
        Res.derivation.addBonus(lambda d: d.getValue(), [gRes], True)
        Aus.derivation.addBonus(lambda d: d.getValue(), [gAus], True)
        Bel.derivation.addBonus(lambda d: 2* d.getValue(), [gBel], True)
        Ini.derivation.addBonus(lambda d: 2 * d.getValue(), [gIni], True)
        LP.derivation.addBonus(lambda d: 5 * d.getValue(), [gLP], True)
        FP.derivation.addBonus(lambda d: d.getValue(), [gFP], True)
        
        ST.costs.add(costFunctionAttributeReduction, gGST)
        KO.costs.add(costFunctionAttributeReduction, gGKO)
        GE.costs.add(costFunctionAttributeReduction, gGGE)
        IN.costs.add(costFunctionAttributeReduction, gGIN)
        WI.costs.add(costFunctionAttributeReduction, gGWI)
        CH.costs.add(costFunctionAttributeReduction, gGCH)
        MU.costs.add(costFunctionAttributeReduction, gGMU)
        WA.costs.add(costFunctionAttributeReduction, gGWA)
        
        # cost reduction for knowledge skills added later
        
        
        disadvantages = []
        disadvantages.append(Disadvantage("Ehrenkodex", 1, lambda v, d: v * -10, None))
        sLP = Disadvantage("Gebrechlich", 1, lambda v, d: v * -10, None)
        sSE1 = Disadvantage("Schlechte Eigenschaft I", 1, lambda v, d: v * -10, None)
        sSE2 = Disadvantage("Schlechte Eigenschaft II", 1, lambda v, d: v * -20, None)
        disadvantages.append(Disadvantage("Schwur", 1, lambda v, d: v * -5, None))
        disadvantages.append(Disadvantage("Sozialer Tollpatsch", 1, lambda v, d: v * -5, None))
        sSU1 = Disadvantage("Sucht I", 1, lambda v, d: v * -10, None)
        sSU2 = Disadvantage("Sucht II", 1, lambda v, d: v * -20, None)
        sSU3 = Disadvantage("Sucht III", 1, lambda v, d: v * -30, None)
        disadvantages.append(Disadvantage("Ungeschickt", 1, lambda v, d: v * -20, None))
        disadvantages.append(Disadvantage("Vergesslich", 1, lambda v, d: v * -10, None))
        self.sLP = sLP
        self.disadvantagesDerivedProperties = [sLP]
        self.disadvantages = disadvantages + self.disadvantagesDerivedProperties + [sSE1, sSE2, sSU1, sSU2, sSU3]
        self.properties.extend(self.disadvantages)
        self.propertiesDict["Schwächen"] = self.disadvantages
        
        # disadvantages constraints
        
        sSE1.constraints.add(constraintFunctionExclusive, [sSE2])
        sSE2.constraints.add(constraintFunctionExclusive, [sSE1])
        sSU1.constraints.add(constraintFunctionExclusive, [      sSU2, sSU3])
        sSU2.constraints.add(constraintFunctionExclusive, [sSU1,       sSU3])
        sSU3.constraints.add(constraintFunctionExclusive, [sSU1, sSU2      ])
        
        # disadvantages effects
        
        LP.derivation.addMalus(lambda d: 5 * d.getValue(), [sLP], True)
        
        
        # skills
        
        self.skills = []
        skillsDict = {}
        self.skillsPhysical = []
        self.skillsPhysical.append(Skill("Akrobatik", [GE], None))
        self.skillsPhysical.append(Skill("Klettern", [ST], None))
        self.skillsPhysical.append(Skill("Reiten", [KO], None))
        self.skillsPhysical.append(Skill("Schleichen", [GE], None))
        self.skillsPhysical.append(Skill("Schwimmen", [KO], None))
        self.skillsPhysical.append(Skill("Springen", [ST], None))
        self.skills.extend(self.skillsPhysical)
        skillsDict["Körperliche Fertigkeiten"] = self.skillsPhysical
        
        self.skillsRethorical = []
        self.skillsRethorical.append(Skill("Betören", [CH], None))
        self.skillsRethorical.append(Skill("Debattieren", [IN, CH], None))
        self.skillsRethorical.append(Skill("Einschüchtern", [CH], None))
        self.skillsRethorical.append(Skill("Feilschen", [IN], None))
        self.skillsRethorical.append(Skill("Lügen", [MU], None))
        self.skills.extend(self.skillsRethorical)
        skillsDict["Rhetorische Fertigkeiten"] = self.skillsRethorical
    
        self.skillsCombat = []
        crossbow = Skill("Armbrust", None, None)
        self.skillsCombat.append(crossbow)
        for skillname in ["Blocken", "Bogen", "Fechtwaffen", 
                          "Wuchtwaffen", "Klingenwaffen", "Schleuder", 
                          "Stangenwaffen", "Waffenloser Kampf", "Wurfwaffen"]:
            self.skillsCombat.append(Skill(skillname, None, None))
        self.skills.extend(self.skillsCombat)
        skillsDict["Kampffertigkeiten"] = self.skillsCombat
        
        crossbow.boundLower.add(lambda: 5, [], False)
        crossbow.costs.add(lambda v, d: -12, None)
        
        self.skillsKnowledge = []
        for skillname in ["Astronomie", "Architektur", "Etikette", 
                          "Gechichten, Mythen & Legenden", "Geographie", "Heraldik", 
                          "Kunde der Vergessenen Kunst", "Mathematik", 
                          "Mechanik", "Pflanzenkunde", "Physiologie", 
                          "Strategie", "Theologie", "Tierkunde", "Wetterkunde"]:
            s = Skill(skillname, [IN], None)
            s.costs.add(costFunctionSkillReduction, gINF) # cost reduction of advantage "Gutes Gedächtnis"
            self.skillsKnowledge.append(s)
        self.skills.extend(self.skillsKnowledge)
        skillsDict["Wissensfertigkeiten"] = self.skillsKnowledge
        
        self.skillsPractical = []
        self.skillsPractical.append(Skill("Alchemie", [self.find("Skills", "Pflanzenkunde")], None))
        self.skillsPractical.append(Skill("Fährtenlesen", [self.find("Skills", "Tierkunde")], None))
        self.skillsPractical.append(Skill("Gassenwissen", None, None))
        self.skillsPractical.append(Skill("Heilkunst", [self.find("Skills", "Physiologie")], None))
        self.skillsPractical.append(Skill("Jagen", [self.find("Skills", "Tierkunde")], None))
        self.skillsPractical.append(Skill("Menschenkenntnis", None, None))
        self.skillsPractical.append(Skill("Schlösseröffnen", [self.find("Skills", "Mechanik")], None))
        self.skillsPractical.append(Skill("Taschendiebstahl", None, None))
        self.skillsPractical.append(Skill("Überlebenskunst", [self.find("Skills", "Pflanzenkunde")], None))
        self.skillsPractical.append(Skill("Verstecken", None, None))
        self.skills.extend(self.skillsPractical)
        skillsDict["Erfahrungsfertigkeiten"] = self.skillsPractical
        
        self.skillsLanguage = []
        for skillname in ["Silirani", "Galathari", "Dhornisch", 
                          "Benesthari (Sprache)", "Maraman", "Alte Sprache"]:
            self.skillsLanguage.append(Skill(skillname, None, None))
        self.skills.extend(self.skillsLanguage)
        skillsDict["Sprachen"] = self.skillsLanguage
        
        self.skillsWriting = []
        for skillname in ["Siliran", "Dhornische Runen", 
                          "Benesthari (Schrift)", "Keilschrift", "Alte Zeichen"]:
            self.skillsWriting.append(Skill(skillname, None, None))
        self.skills.extend(self.skillsWriting)
        skillsDict["Schriften"] = self.skillsWriting
        
        self.skillsAncientArts = []
        for skillname in ["Allsicht", "Ordokinetik", "Inneres Chaos", "Elementare Dualität"]:
            self.skillsAncientArts.append(Skill(skillname, [IN], None))
        self.skills.extend(self.skillsAncientArts)
        skillsDict["Vergessene Kunst"] = self.skillsAncientArts
        
        self.skillsWondersPantheon = []
        for skillname in ["Das himmlische Gesetz (Ceïros)", "Das Buch der Vorsehung (Elunae)", "Atlas der Mysterien (Midras)", "Der göttliche Entwurf (Katho)", "Kompositionen der Ästhetik (Alaya)"]:
            self.skillsWondersPantheon.append(Skill(skillname, [WI], None))
        self.skills.extend(self.skillsWondersPantheon)
        skillsDict["Lehren der fünf Schöpfer"] = self.skillsWondersPantheon
        
        self.properties.extend(self.skills)
        self.propertiesDict["Fertigkeiten"] = skillsDict
        self.propertiesDict.update(skillsDict)


        self.talents = []
        self.talentsWeapons = []
        talentsDict = {}
        # name, boundUpper, costsFunction, assostiatedBounds, assosiatedValues, tooltip
        self.talentsWeapons.append(Talent("Guter Schütze", 3, 
                                           lambda v, d: ([0, 10, 30, 60])[v],
                                           [5, 7, 9],
                                           [self.find("Skills", "Armbrust"),
                                            self.find("Skills", "Bogen"),
                                            self.find("Skills", "Wurfwaffen")],
                                           None))

        self.talentsWeapons.append(Talent("Waffe und Schild", 3, 
                                          lambda v, d: ([0, 10, 30, 60])[v],
                                          [5, 7, 9],
                                          [self.find("Skills", "Blocken")],
                                          None))
        self.talentsWeapons.append(Talent("Kampf mit zwei Waffen", 3, 
                                          lambda v, d: ([0, 10, 30, 60])[v],
                                          [4, 6, 7],
                                          [GE],
                                          None))
        self.talentsWeapons.append(Talent("Kampfreflexe", 1, 
                                          lambda v, d: ([0, 20])[v], 
                                          [6],
                                          [GE],
                                          None))
        self.talentsWeapons.append(Talent("Rüstungsgewöhnung", 3, 
                                          lambda v, d: ([0, 10, 30, 60])[v],
                                          [4, 6, 7],
                                          [ST],
                                          None))
        self.talentsWeapons.append(Talent("Passierschlag", 1, 
                                          lambda v, d: ([0, 40])[v], 
                                          [11],
                                          [Ini],
                                          None))
        self.talentsWeapons.append(Talent("Schnelles Auflegen", 1, 
                                          lambda v, d: ([0, 15])[v], 
                                          [7],
                                          [self.find("Skills", "Bogen")],
                                          None))
        self.talentsWeapons.append(Talent("Schnelles Nachladen", 1, 
                                          lambda v, d: ([0, 30])[v],
                                          [7],
                                          [self.find("Skills", "Armbrust")],
                                          None))
        self.talentsWeapons.append(Talent("Schnelles Schiessen / Werfen", 1, 
                                          lambda v, d: ([0, 30])[v], 
                                          [7],
                                          [self.find("Skills", "Bogen"),
                                           self.find("Skills", "Wurfwaffen")],
                                          None))
        self.talentsWeapons.append(Talent("Zäher Hund", 2, 
                                          lambda v, d: ([0, 20, 60])[v], 
                                          [5, 7],
                                          [KO],
                                          None))
        self.talents.extend(self.talentsWeapons)
        talentsDict["Waffentalente"] = self.talentsWeapons
        
        self.talentsAncientArt = []
        self.talentsAncientArt.append(Talent("Flüsse und Wirbel", 1, 
                                             lambda v, d: ([0, 30])[v],
                                             [4],
                                             [self.find("Skills", "Ordokinetik")],
                                             None))
        self.talentsAncientArt.append(Talent("Konzentration", 1, 
                                             lambda v, d: ([0, 30])[v], 
                                             [0],
                                             [],
                                             None))
        self.talentsAncientArt.append(Talent("Wechselwarm", 1, 
                                             lambda v, d: ([0, 40])[v], 
                                             [6],
                                             [self.find("Skills", "Inneres Chaos")],
                                             None))
        self.talents.extend(self.talentsAncientArt)
        talentsDict["Arkana"] = self.talentsAncientArt
        
        self.talentsWonders = []
        self.talentsWonders.append(Talent("Empyreische Gestalt", 2, 
                                          lambda v, d: ([0, 20, 60])[v],
                                          [6, 8],
                                          [CH],
                                          None))
        self.talentsWonders.append(Talent("Hohe Weihe", 1, 
                                          lambda v, d: ([0, 40])[v], 
                                          [6, 8],
                                          self.skillsWondersPantheon,
                                          None))        
        self.talents.extend(self.talentsWonders)
        talentsDict["Wunder"] = self.talentsWonders
        
        self.properties.extend(self.talents)
        self.propertiesDict["Besondere Talente"] = talentsDict
        self.propertiesDict.update(talentsDict)

        
        # equipment
        
        self.equipment = Equipment(self)
    
        # add effects of character properties regarding equipment
        self.equipment.BehA.derivation.addMalus(lambda d: ([0, 2, 4, 6])[d.getValue()], [self.find("Talents", "Rüstungsgewöhnung")], False)
        self.equipment.BehS.derivation.addMalus(lambda d: ([0, 1, 3, 5])[d.getValue()], [self.find("Talents", "Waffe und Schild")], False)
        self.Bel.dependingProperties.add(self.equipment.Beh)
    
    def find_uuid(self, uuid):
        for e in self.properties:
            if e.uuid == uuid:
                return e
        return None
    
    def find(self, subcategory, name):
        characterProperties = []
        if subcategory == "Attributes":
            characterProperties = self.attributes
        elif subcategory == "Derived Properties":
            characterProperties = self.derivedProperties
        elif subcategory == "Advantages":
            characterProperties = self.advantages
        elif subcategory == "Disadvantages":
            characterProperties = self.disadvantages
        elif subcategory == "Skills":
            characterProperties = self.skills
        elif subcategory == "Talents":
            characterProperties = self.talents

        for e in characterProperties:
            if e.name == name or e.shortname == name:
                return e
        return None
    
    def costs(self, propertiesName):
        if not propertiesName or propertiesName == "":
            characterProperties = self.properties
        else:
            characterProperties = self.propertiesDict[propertiesName]
        costs = 0
        if isinstance(characterProperties, dict):
            for name in characterProperties:
                costs = costs + self.costs(name)
        else:
            for e in characterProperties:
                costs = costs + e.getCosts()
        return costs
    
    def output(self):
        for e in self.properties:
            if type(e) != DerivedProperty:
                print(e.shortname, e.getValue())
    
    def save(self):
        values = []
        for e in self.properties:
            if type(e) != DerivedProperty:
                values.append(e.getValue())
        return values
    
    def load(self, values, updateUI=True):
        for e in self.properties:
            if type(e) != DerivedProperty:
                e.setValue(0, updateUI)
        index = 0
        for e in self.properties:
            if type(e) != DerivedProperty:
                e.setValue(values[index], updateUI)
                index = index + 1
    
    def uiEvent(self, category, uuid, value):
        if category == "Character":
            e = self.find_uuid(uuid)
            e.setValue(value, True)
            if not isinstance(e, DerivedPropertyBound):
                self.uiUpdateXP()
                self.equipment.uiWeapons.update()
        elif category == "Equipment":
            self.equipment.uiEvent(uuid, value)
            
    def uiUpdateXP(self):
        for propertiesName in self.uiXP:
            for ui in self.uiXP[propertiesName]:
                ui.update(str(self.costs(propertiesName)))
    
    def createUIXPEntity(self, propertiesName):
        if not propertiesName in self.uiXP:
            self.uiXP[propertiesName] = []
        text = psg.Text(str(self.costs(propertiesName)))
        self.uiXP[propertiesName].append(text)
        return text
            
    def trial(self, uuids):
        trials = []
        propertyNames = []
        for uuid in uuids:
            e = self.find_uuid(uuid)
            trials.append(e.trial)
            propertyNames.append(e.name)
        multiTrial = MultiTrial(trials)
        return multiTrial.roll()
            