from classes.ValueProperty import ValueProperty

class Armour(ValueProperty):
    def __init__(self, name, armourType, armourFactor, armourRating, encumbrance, constraintDependencies):
        ValueProperty.__init__(self, "Equipment", armourType, name, None, name, 0, 1, 1)
        self.armourType = armourType
        self.armourFactor = armourFactor
        self.armourRating = armourRating
        self.encumbrance = encumbrance
        # add constraint that only one item per body part can be active
        self.constraints.add(lambda v, d: v == 0 or sum([a.getValue() for a in d]) == 0, constraintDependencies)
        
        self.bonusArmourRating = 0.25
        self.iniMalus = 0
    
    def armourRatingFull(self):
        return self.bonusArmourRating * (self.armourRating > 0)
    
    def armourRatingFactored(self):            
        return self.armourFactor * self.armourRating

class Fullbody(Armour):
    def __init__(self, name, armourRating, encumbrance, iniMalus, constraintDependencies):
        Armour.__init__(self, name, "Voll", 1, armourRating, encumbrance, constraintDependencies)
        self.iniMalus = iniMalus
        self.bonusArmourRating = 2

class Head(Armour):
    def __init__(self, name, armourRating, iniMalus, constraintDependencies):
        Armour.__init__(self, name, "Kopf", 0.2, armourRating, 0, constraintDependencies)
        self.iniMalus = iniMalus

class Chest(Armour):
    def __init__(self, name, armourRating, encumbrance, constraintDependencies):
        Armour.__init__(self, name, "Torso", 0.4, armourRating, encumbrance, constraintDependencies)
        
class Arms(Armour):
    def __init__(self, name, armourRating, encumbrance, constraintDependencies):
        Armour.__init__(self, name, "Arme", 0.2, armourRating, encumbrance, constraintDependencies)

class Legs(Armour):
    def __init__(self, name, armourRating, encumbrance, constraintDependencies):
        Armour.__init__(self, name, "Beine", 0.2, armourRating, encumbrance, constraintDependencies)