from classes.CharacterProperty import DerivedPropertyBound, DerivedProperty
from classes.Armour import Fullbody, Head, Chest, Arms, Legs
from classes.Weapon import Weapon, Bow, Shield
from classes.DiceRoll import DiceRoll_1D4, DiceRoll_2D4, DiceRoll_1D6, DiceRoll_2D6, DiceRoll_2D8, DiceRoll_2D10, DiceRoll_2D12
from classes.UI import UIEquippedWeaponStats
from classes.Trial import MultiTrial

class Equipment:
    def __init__(self, character):
        self.character = character       
        
        self.properties = []
        
        # Armour
        
        self.armours = []
        self.armoursFull = []
        self.armoursHead = []
        self.armoursChest = []
        self.armoursArms = []
        self.armoursLegs = []
        
        # Full
        
        # name, armourRating, encumbrance, iniMalus, constraintFunction, constraintDependenciesWeaponsShields
        self.armoursFull.append(Fullbody("Kettenrüstung",   5,  7, 1, self.armours))
        self.armoursFull.append(Fullbody("Feldharnisch",   12, 18, 2, self.armours))
        self.armoursFull.append(Fullbody("Gestechrüstung", 14, 20, 3, self.armours))
        
        # Head
        dependencies = []
        # name, armourRating, iniMalus, constraintFunction, constraintDependenciesWeaponsShields
        self.armoursHead.append(Head("Hut",             0, 0, dependencies))
        self.armoursHead.append(Head("Wattierte Haube", 2, 0, dependencies))
        self.armoursHead.append(Head("Lederhelm",       3, 0, dependencies))
        self.armoursHead.append(Head("Kettenhaube",     5, 1, dependencies))
        self.armoursHead.append(Head("Nasalhelm",       6, 2, dependencies))
        self.armoursHead.append(Head("Brillenhelm",     6, 2, dependencies))
        self.armoursHead.append(Head("Eisenhut",        6, 2, dependencies))
        self.armoursHead.append(Head("Topfhelm",        8, 3, dependencies))
        dependencies.extend(self.armoursFull + self.armoursHead)
        
        # Chest
        dependencies = []
        # name, armourRating, encumbrance, constraintFunction, constraintDependenciesWeaponsShields
        self.armoursChest.append(Chest("Normale Kleidung",  0, 0, dependencies))
        self.armoursChest.append(Chest("Gambeson",          2, 0, dependencies))
        self.armoursChest.append(Chest("Lederrüstung",      3, 1, dependencies))
        self.armoursChest.append(Chest("Kettenrüstung",     5, 2, dependencies))
        self.armoursChest.append(Chest("Brigantine",        6, 3, dependencies))
        self.armoursChest.append(Chest("Schuppenrüstung",   8, 4, dependencies))
        self.armoursChest.append(Chest("Plattenrüstung",   10, 5, dependencies))
        dependencies.extend(self.armoursFull + self.armoursChest)
        # Arms
        dependencies = []
        # name, armourRating, encumbrance, constraintFunction, constraintDependenciesWeaponsShields
        self.armoursArms.append(Arms("Normale Kleidung",  0, 0, dependencies))
        self.armoursArms.append(Arms("Gambeson",          2, 1, dependencies))
        self.armoursArms.append(Arms("Lederrüstung",      3, 2, dependencies))
        self.armoursArms.append(Arms("Kettenrüstung",     5, 3, dependencies))
        self.armoursArms.append(Arms("Brigantine",        6, 4, dependencies))
        self.armoursArms.append(Arms("Schuppenrüstung",   8, 5, dependencies))
        self.armoursArms.append(Arms("Plattenrüstung",   10, 6, dependencies))
        dependencies.extend(self.armoursFull + self.armoursArms)
        # Legs
        dependencies = []
        # name, armourRating, encumbrance, constraintFunction, constraintDependenciesWeaponsShields
        self.armoursLegs.append(Legs("Normale Kleidung",  0, 0, dependencies))
        self.armoursLegs.append(Legs("Gambeson",          2, 1, dependencies))
        self.armoursLegs.append(Legs("Lederrüstung",      3, 2, dependencies))
        self.armoursLegs.append(Legs("Kettenrüstung",     5, 3, dependencies))
        self.armoursLegs.append(Legs("Brigantine",        6, 4, dependencies))
        self.armoursLegs.append(Legs("Schuppenrüstung",   8, 5, dependencies))
        self.armoursLegs.append(Legs("Plattenrüstung",   10, 6, dependencies))
        dependencies.extend(self.armoursFull + self.armoursLegs)
        
        self.armours.extend(self.armoursFull + self.armoursHead + self.armoursChest + self.armoursArms + self.armoursLegs)
        
        self.properties.extend(self.armours)
        
        def constraintFunctionArmourIniMalus(value, dependencies):
            boundLower = dependencies[0].boundLower.getValue()
            iniMalus = dependencies[1].iniMalus
            if value == 0 or iniMalus == 0 or boundLower == 0:
                return True
            return boundLower <= dependencies[0].getValue() - iniMalus
        
        # add constraint to armour preventing an Ini-malus to lower Ini below its lower bound
        for a in self.armours:
            a.constraints.add(constraintFunctionArmourIniMalus, [self.character.Ini, a])
        
        
        # shields
        
        self.shields = []
        
        constraintDependenciesWeaponsShields = [] #filled later with shields and weapons
        
        # name, encumbrance, shieldBonus, stability, coverRating, constraintFunction, constraintDependenciesWeaponsShields
        
        self.shields.append(Shield("Tartsche",                2, 1, 2,  0, constraintDependenciesWeaponsShields))
        self.shields.append(Shield("Kleiner Schild",          3, 1, 5, 25, constraintDependenciesWeaponsShields))
        self.shields.append(Shield("Holzschild",              4, 2, 6, 50, constraintDependenciesWeaponsShields))
        self.shields.append(Shield("Verstärkter Schild",      5, 2, 8, 50, constraintDependenciesWeaponsShields))
        self.shields.append(Shield("Rundschild",              5, 2, 6, 75, constraintDependenciesWeaponsShields))
        self.shields.append(Shield("Beschlagener Rundschild", 6, 2, 8, 75, constraintDependenciesWeaponsShields))
        self.shields.append(Shield("Turmschild",              6, 3, 6, 75, constraintDependenciesWeaponsShields))
        self.shields.append(Shield("Beschlagener Turmschild", 7, 3, 8, 75, constraintDependenciesWeaponsShields))
        
        constraintDependenciesWeaponsShields.extend(self.shields)
        self.properties.extend(self.shields)

        maximumArmourEncumbrance = max([a.encumbrance for a in self.armoursFull] + [a.encumbrance * 4 for a in self.armoursChest + self.armoursArms + self.armoursLegs + self.armoursHead])
        maximumShieldEncumbrance = max([s.encumbrance for s in self.shields])
        maximumArmourRatingHead = max([a.armourRating for a in self.armoursHead + self.armoursFull])
        maximumArmourRatingChest = max([a.armourRating for a in self.armoursChest + self.armoursFull])
        maximumArmourRatingArms = max([a.armourRating for a in self.armoursArms + self.armoursFull])
        maximumArmourRatingLegs = max([a.armourRating for a in self.armoursLegs + self.armoursFull])
        maximumArmourRating = 0.2 *(maximumArmourRatingHead + maximumArmourRatingArms + maximumArmourRatingLegs) + 0.4 * maximumArmourRatingChest
        maximumArmourRatingFull = max(a.bonusArmourRating for a in self.armours)


        # derived properties of armour and shield
        # category, subcategory, name, shortname, boundLower, boundUpper, boundStep, derivationFunction, derivationDependencies, tooltip
        self.BehA = DerivedPropertyBound("Equipment", "Derived Properties", "Rüstungsbehinderung", "Rüstbeh.", 
                                         0, maximumArmourEncumbrance, 1, 
                                         lambda *armours: sum([a.getValue() * a.encumbrance for a in armours]), 
                                         self.armours)
        self.BehS = DerivedPropertyBound("Equipment", "Derived Properties","Schildbehinderung", "Schildbeh.",
                                         0, maximumShieldEncumbrance, 1, 
                                         lambda *armours: sum([a.getValue() * a.encumbrance for a in armours]), 
                                         self.shields)
        self.Beh = DerivedPropertyBound("Equipment", "Derived Properties","Effektive Behinderung", "Eff. Beh.", 
                                        -100, 100, 1, 
                                        lambda BehA, BehS, Bel: BehA.getValue() + BehS.getValue() - Bel.getValue(), 
                                        [self.BehA, self.BehS, self.character.Bel])
        self.derivedProperties = [self.BehA, self.BehS, self.Beh]
        
        # armour rating
        
        self.RS = DerivedPropertyBound("Equipment", "Derived Properties","Rüstschutz", "RS", 
                                       0, maximumArmourRating, 1,
                                       lambda *armours: sum([a.getValue() * a.armourRatingFactored() for a in armours]), 
                                       self.armours)
        self.RSK = DerivedPropertyBound("Equipment", "Derived Properties","Rüstschutz Kopf", "RS-K", 
                                        0, maximumArmourRatingHead, 1, 
                                        lambda *armours: sum([a.getValue() * a.armourRating for a in armours]), 
                                        self.armoursHead + self.armoursFull)
        self.RST = DerivedPropertyBound("Equipment", "Derived Properties","Rüstschutz Torso", "RS-T", 
                                        0, maximumArmourRatingChest, 1, 
                                        lambda *armours: sum([a.getValue() * a.armourRating for a in armours]), 
                                        self.armoursChest + self.armoursFull)
        self.RSA = DerivedPropertyBound("Equipment", "Derived Properties","Rüstschutz Arme", "RS-A", 
                                        0, maximumArmourRatingArms, 1, 
                                        lambda *armours: sum([a.getValue() * a.armourRating for a in armours]), 
                                        self.armoursArms + self.armoursFull)
        self.RSB = DerivedPropertyBound("Equipment", "Derived Properties","Rüstschutz Beine", "RS-B", 
                                        0, maximumArmourRatingLegs, 1, 
                                        lambda *armours: sum([a.getValue() * a.armourRating for a in armours]), 
                                        self.armoursLegs + self.armoursFull)
        self.RSV = DerivedPropertyBound("Equipment", "Derived Properties","Rüstschutz Voll", "RS-V", 
                                        0, maximumArmourRatingFull, 1, 
                                        lambda *armours: int(sum([a.getValue() * a.armourRatingFull() for a in armours])), 
                                        self.armours)
        
        self.armourRatings = [self.RS, self.RSK, self.RST, self.RSA, self.RSB, self.RSV]
        self.derivedProperties.extend(self.armourRatings)
    
        self.properties.extend(self.derivedProperties)
    
        # ini modificator
        self.character.Ini.derivation.addMalus(lambda *armours: sum([a.getValue() * a.iniMalus for a in armours]), self.armoursFull + self.armoursHead, False)

        
        # weapons    
        
        self.weapons = []
        self.weaponsMH = []
        skills = [self.character.find("Skills", "Klingenwaffen")]
        # character, name, skills, isTwohanded, STRequirement, constraintDependenciesWeaponsShields, diceRoll, damageBonus, reach, IPCostsForAttackCycle, tooltip
        self.weaponsMH.append(Weapon(self.character, "Messer", skills, False, 1, constraintDependenciesWeaponsShields, DiceRoll_1D4, 0))
        self.weaponsMH.append(Weapon(self.character, "Dolch", skills, False, 2, constraintDependenciesWeaponsShields, DiceRoll_2D4, 0))
        self.weaponsMH.append(Weapon(self.character, "Stoßdolch", skills, False, 2, constraintDependenciesWeaponsShields, DiceRoll_2D4, 0))
        self.weaponsMH.append(Weapon(self.character, "Kurzschwert", skills, False, 3, constraintDependenciesWeaponsShields, DiceRoll_2D6, 0))
        self.weaponsMH.append(Weapon(self.character, "Falchion", skills, False, 3, constraintDependenciesWeaponsShields, DiceRoll_2D6, 0))
        self.weaponsMH.append(Weapon(self.character, "Ritterschwert", skills, False, 4, constraintDependenciesWeaponsShields, DiceRoll_2D6, 1))
        self.weaponsMH.append(Weapon(self.character, "Langschwert", skills, True, 5, constraintDependenciesWeaponsShields, DiceRoll_2D6, 2))
        self.weaponsMH.append(Weapon(self.character, "Krummschwert", skills, True, 5, constraintDependenciesWeaponsShields, DiceRoll_2D6, 2))
        
        skills = [self.character.find("Skills", "Fechtwaffen")]
        self.weaponsMH.append(Weapon(self.character, "Degen", skills, False, 3, constraintDependenciesWeaponsShields, DiceRoll_2D6, 0))
        
        skills = [self.character.find("Skills", "Klingenwaffen"),
                  self.character.find("Skills", "Fechtwaffen")]
        self.weaponsMH.append(Weapon(self.character, "Rapier", skills, False, 3, constraintDependenciesWeaponsShields, DiceRoll_2D6, 0))
        self.weaponsMH.append(Weapon(self.character, "Säbel", skills, False, 4, constraintDependenciesWeaponsShields, DiceRoll_2D6, 1))
        
        skills = [self.character.find("Skills", "Wuchtwaffen")]
        self.weaponsMH.append(Weapon(self.character, "Stock", skills, False, 2, constraintDependenciesWeaponsShields, DiceRoll_2D4, -2))
        self.weaponsMH.append(Weapon(self.character, "Holzkeule", skills, False, 3, constraintDependenciesWeaponsShields, DiceRoll_2D4, 0))
        self.weaponsMH.append(Weapon(self.character, "Streitkolben", skills, False, 5, constraintDependenciesWeaponsShields, DiceRoll_2D10, -1))
        self.weaponsMH.append(Weapon(self.character, "Rabenschnabel", skills, False, 5, constraintDependenciesWeaponsShields, DiceRoll_2D8, 0))
        self.weaponsMH.append(Weapon(self.character, "Kriegsbeil", skills, False, 5, constraintDependenciesWeaponsShields, DiceRoll_2D8, 0))
        self.weaponsMH.append(Weapon(self.character, "Morgenstern", skills, True, 6, constraintDependenciesWeaponsShields, DiceRoll_2D12, -1))
        self.weaponsMH.append(Weapon(self.character, "Streitaxt", skills, True, 6, constraintDependenciesWeaponsShields, DiceRoll_2D10, 0))
        self.weaponsMH.append(Weapon(self.character, "Streithammer", skills, True, 6, constraintDependenciesWeaponsShields, DiceRoll_2D10, 0))
        self.weaponsMH.append(Weapon(self.character, "Bidenhänder", skills, True, 7, constraintDependenciesWeaponsShields, DiceRoll_2D12, 0, 2))
        
        skills = [self.character.find("Skills", "Stangenwaffen")]
        self.weaponsMH.append(Weapon(self.character, "Kampfstab", skills, True, 3, constraintDependenciesWeaponsShields, DiceRoll_2D6, 0, 2))
        self.weaponsMH.append(Weapon(self.character, "Holzspeer", skills, True, 3, constraintDependenciesWeaponsShields, DiceRoll_2D8, 0, 2))
        self.weaponsMH.append(Weapon(self.character, "Stoßspeer", skills, True, 4, constraintDependenciesWeaponsShields, DiceRoll_2D10, -1, 2))
        self.weaponsMH.append(Weapon(self.character, "Glefe", skills, True, 5, constraintDependenciesWeaponsShields, DiceRoll_2D10, 0, 2))
        self.weaponsMH.append(Weapon(self.character, "Hellebarde", skills, True, 6, constraintDependenciesWeaponsShields, DiceRoll_2D10, 0, 2))
        
        skills = [self.character.find("Skills", "Bogen")]
        self.weaponsBows = []
        self.weaponsBows.append(Bow(self.character, "Kurzbogen (45Pf)", skills, True, 2, constraintDependenciesWeaponsShields, DiceRoll_2D4, 0, 7, 10))
        self.weaponsBows.append(Bow(self.character, "Kurzbogen (60Pf)", skills, True, 4, constraintDependenciesWeaponsShields, DiceRoll_2D6, -1, 9, 10))
        self.weaponsBows.append(Bow(self.character, "Kurzbogen (90Pf)", skills, True, 5, constraintDependenciesWeaponsShields, DiceRoll_2D6, 0, 11, 10))
        self.weaponsBows.append(Bow(self.character, "Kompositbogen (45Pf)", skills, True, 2, constraintDependenciesWeaponsShields, DiceRoll_2D6, -1, 7, 10))
        self.weaponsBows.append(Bow(self.character, "Kompositbogen (60Pf)", skills, True, 4, constraintDependenciesWeaponsShields, DiceRoll_2D6, 0, 9, 10))
        self.weaponsBows.append(Bow(self.character, "Kompositbogen (90Pf)", skills, True, 5, constraintDependenciesWeaponsShields, DiceRoll_2D8, -1, 11, 10))
        self.weaponsBows.append(Bow(self.character, "Kompositbogen (120Pf)", skills, True, 6, constraintDependenciesWeaponsShields, DiceRoll_2D8, 0, 13, 10))
        self.weaponsBows.append(Bow(self.character, "Langbogen (60Pf)", skills, True, 4, constraintDependenciesWeaponsShields, DiceRoll_2D6, -1, 9, 10))
        self.weaponsBows.append(Bow(self.character, "Langbogen (90Pf)", skills, True, 5, constraintDependenciesWeaponsShields, DiceRoll_2D6, 0, 11, 10))
        self.weaponsBows.append(Bow(self.character, "Langbogen (120Pf)", skills, True, 6, constraintDependenciesWeaponsShields, DiceRoll_2D6, 1, 13, 10))
        self.weaponsBows.append(Bow(self.character, "Langbogen (150Pf)", skills, True, 7, constraintDependenciesWeaponsShields, DiceRoll_2D6, 2, 15, 10))
        self.weaponsMH.extend(self.weaponsBows)
        
        skills = [self.character.find("Skills", "Armbrust")]
        wLCB = Bow(self.character, "Leichte Armbrust", skills, True, 0, constraintDependenciesWeaponsShields, DiceRoll_2D4, 0, 6, 10)
        wCB = Bow(self.character, "Armbrust", skills, True, 0, constraintDependenciesWeaponsShields, DiceRoll_2D6, 0, 11, 16)
        wHCB = Bow(self.character, "Schwere Armbrust", skills, True, 0, constraintDependenciesWeaponsShields, DiceRoll_2D6, 2, 15, 22)
        self.weaponsMH.extend([wLCB, wCB, wHCB])
        
        skills = [self.character.find("Skills", "Wurfwaffen")]
        self.weaponsThrowing = []
        self.weaponsThrowing.append(Bow(self.character, "Wurfmesser", skills, False, 0, constraintDependenciesWeaponsShields, DiceRoll_1D6, 0, 3, 7))
        self.weaponsThrowing.append(Bow(self.character, "Wurfdolch", skills, False, 0, constraintDependenciesWeaponsShields, DiceRoll_2D4, 0, 3, 7))
        self.weaponsThrowing.append(Bow(self.character, "Wurfaxt", skills, False, 0, constraintDependenciesWeaponsShields, DiceRoll_2D4, 0, 3, 7))
        self.weaponsThrowing.append(Bow(self.character, "Wurfspeer", skills, False, 0, constraintDependenciesWeaponsShields, DiceRoll_2D6, 0, 4, 7))
        self.weaponsMH.extend(self.weaponsThrowing)
        
        skills = [self.character.find("Skills", "Schleuder")]
        self.weaponsMH.append(Weapon(self.character, "Schleuder", skills, True, 0, constraintDependenciesWeaponsShields, DiceRoll_2D4, 0, 4))
        
        self.weapons.extend(self.weaponsMH)
    
        self.weaponsOH = []
        self.weaponsOH.extend([w.copy(self.weapons) for w in self.weapons if not w.isTwohanded])
        
        self.weapons.extend(self.weaponsOH)
        
        constraintDependenciesWeaponsShields.extend(self.weapons)
        
        self.properties.extend(self.weapons)
            
        # effects of talents on IP costs of weapons
        
        talent = self.character.find("Talents", "Schnelles Auflegen")
        for w in self.weaponsBows:
            w.IPCostsForAttackCycle.add(lambda v, d: -d.getValue(), talent)
        
        talent = self.character.find("Talents", "Schnelles Nachladen")
        wLCB.IPCostsForAttackCycle.add(lambda v, d: -d.getValue(), talent)
        wCB.IPCostsForAttackCycle.add(lambda v, d: -d.getValue()*2, talent)
        wHCB.IPCostsForAttackCycle.add(lambda v, d: -d.getValue()*3, talent)
            
        talent = self.character.find("Talents", "Schnelles Schiessen / Werfen")
        for w in self.weaponsBows + self.weaponsThrowing:
            w.IPCostsForAttackCycle.add(lambda v, d: -d.getValue(), talent)

        # combat values and settings
        
        # category, subcategory, name, shortname, boundLower, boundUpper, boundStep, derivationFunction, derivationDependencies, tooltip
        self.dualWield = DerivedPropertyBound("Equipment", "Internal", "IsDualWield", "IsDW", 0, 1, 1,
                                               lambda *w: sum([x.getValue() for x in w]) > 0, 
                                               self.weaponsOH)
        
        self.mainhandMalus = DerivedPropertyBound("Equipment", "Internal", "MainhandMalus", "MhMalus", 0, 2, 1, 
                                             lambda isDW, talentDW : isDW.getValue() * ([2, 1, 1, 0])[talentDW.getValue()], 
                                             [self.dualWield, self.character.find("Talents", "Kampf mit zwei Waffen")])
        self.offhandMalus = DerivedPropertyBound("Equipment", "Internal", "OffhandHandMalus", "OhMalus", 1, 2, 1, 
                                             lambda isDW, talentDW : isDW.getValue() * ([2, 2, 1, 1])[talentDW.getValue()], 
                                             [self.dualWield, self.character.find("Talents", "Kampf mit zwei Waffen")])
        self.shieldBonus = DerivedPropertyBound("Equipment", "Internal", "ShieldBonus", "SBonus", 0, 3, 3, 
                                             lambda *shields :  sum([s.getValue() * s.shieldBonus for s in shields]), 
                                             self.shields)
        self.internal = [self.dualWield, self.mainhandMalus, self.offhandMalus]
        
        self.properties.extend(self.internal)

        # weaponsMH, weaponsOH, category, uuid, name, tooltip
        self.uiWeapons = UIEquippedWeaponStats(self.weaponsMH, self.weaponsOH, "Equipment", "Internal", None, "WeaponStats")

    def find_uuid(self, uuid):
        for e in self.properties:
            if e.uuid == uuid:
                return e
        return None
    
    def find(self, subcategory, name):
        equipment = []
        if subcategory == "Schild":
            equipment = self.shields
        elif subcategory == "Voll":
            equipment = self.armoursFull
        elif subcategory == "Kopf":
            equipment = self.armoursHead
        elif subcategory == "Torso":
            equipment = self.armoursChest
        elif subcategory == "Arme":
            equipment = self.armoursArms
        elif subcategory == "Beine":
            equipment = self.armoursLegs
        elif subcategory == "Internal":
            equipment = self.internal
        for e in equipment:
            if e.name == name:
                return e
        return None
        
    def uiEvent(self, uuid, value):
        self.find_uuid(uuid).setValue(value, True)
        for e in self.derivedProperties:
            e.UI.update()
        self.uiWeapons.update()
        
    def save(self):
        values = []
        for e in self.armours + self.shields:
            values.append(e.getValue())
        return values
            
    def load(self, values):
        for v, e in zip(values, self.armours + self.shields):
            e.setValue(v, True)   
            
    def trialCombat(self):
        weapons = []
        isMainhand = False
        isOffhand = False
        outcome = []
        damages = []
        for w in self.weaponsMH:
            if w.getValue():
                isMainhand = True
                weapons.append(w)
                break
        for w in self.weaponsOH:
            if w.getValue():
                isOffhand = True
                weapons.append(w)
                break        
        if weapons != [] and (not isOffhand or isMainhand):
            trial = MultiTrial(weapons)
            outcome.extend(trial.roll())
            damages = [w.damage() for w in weapons]
        return outcome, damages
            
            