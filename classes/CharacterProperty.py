from classes.Derivation import Derivation
from classes.ValueProperty import ValueProperty
from classes.Trial import Trial

class ValuePropertyWithTrial(ValueProperty):
    def __init__(self, category, subcategory, name, tooltip, shortname, boundLower, boundUpper, boundStep):
        ValueProperty.__init__(self, category, subcategory, name, tooltip, shortname, boundLower, boundUpper, boundStep)
        self.trial = Trial(self)

class DerivedPropertyBound(ValuePropertyWithTrial):
 def __init__(self, category, subcategory, name, shortname, boundLower, boundUpper, boundStep, derivationFunction, derivationDependencies, tooltip = None):
     ValuePropertyWithTrial.__init__(self, category, subcategory, name, tooltip, shortname, boundLower, boundUpper, boundStep)
     self.derivation = Derivation(self, derivationFunction, derivationDependencies)
     
 def setValue(self, value, updateUI):
     ValuePropertyWithTrial.setValue(self, value, False)

 # override
 def getValue(self, rounding=True):
     return min(max(self.derivation.calculate(rounding), self.boundLower.getValue()), self.boundUpper.getValue())

class DerivedProperty(DerivedPropertyBound):
    def __init__(self, name, shortname, boundStep, derivationFunction, derivationDependencies, tooltip = None):
        DerivedPropertyBound.__init__(self, "Character", "Derived Properties", name, shortname, -100, 100, boundStep, derivationFunction, derivationDependencies, tooltip)
        self.boundLower.add(self.derivation.getMinimum, [], False)
        self.boundUpper.add(self.derivation.getMaximum, [], False)
        
class PurchaseableCharacterPropterty(ValuePropertyWithTrial):
    def __init__(self, category, subcategory, name, tooltip, shortname, boundLower, boundUpper, boundStep, costsFunction, costDependencies):
        ValuePropertyWithTrial.__init__(self, category, subcategory, name, tooltip, shortname, boundLower, boundUpper, boundStep)
        self.costs.add(costsFunction, costDependencies)
    
class Attribute(PurchaseableCharacterPropterty):
    def __init__(self, name, shortname, tooltip):
        PurchaseableCharacterPropterty.__init__(self, "Character", "Attributes", name, tooltip, shortname, 0, 10, 1, lambda v, d: ([0, 2, 5, 10, 18, 31, 52, 86, 141, 230, 374])[v], None)
        
class Advantage(PurchaseableCharacterPropterty):
    def __init__(self, name, boundUpper, costsFunction, tooltip):
        PurchaseableCharacterPropterty.__init__(self, "Character", "Advantages", name, tooltip, name, 0, boundUpper, 1, costsFunction, None)

class Disadvantage(PurchaseableCharacterPropterty):
    def __init__(self, name, boundUpper, costsFunction, tooltip):
        PurchaseableCharacterPropterty.__init__(self, "Character", "Disadvantages", name, tooltip, name, 0, boundUpper, 1, costsFunction, None)

def boundConstraintFunctionDependingValue(*assosiatedValues):
    return max([0] + [e.getValue() for e in assosiatedValues]) + 1

def boundConstraintFunctionAssosiatedValue(dependingValue, assosiatedValues):
    maxValue = max([0] + [e.getValue() for e in assosiatedValues])
    if maxValue >= dependingValue.getValue() - 1:
        return -100 # lower bound not set if depending value is 0 or bound is fulfilled by other assosiated value
    return dependingValue.getValue() - 1


class Skill(PurchaseableCharacterPropterty):
    def __init__(self, name, assosiatedValues, tooltip):
        PurchaseableCharacterPropterty.__init__(self, "Character", "Skills", name, tooltip, name, 0, 10, 1, lambda v, d: ([0, 1, 2, 4, 7, 12, 20, 33, 54, 88, 143])[v], None)
        if assosiatedValues:
            self.boundUpper.add(boundConstraintFunctionDependingValue, assosiatedValues, False)
            for e in assosiatedValues:
                e.boundLower.add(boundConstraintFunctionAssosiatedValue, [self, [x for x in assosiatedValues if x != e]], False)

def boundConstraintFunctionDependingValueThreshold(assosiatedValues, thresholds):
    maxValue = max([0] + [e.getValue() for e in assosiatedValues])
    for i in range(len(thresholds)-1, -1, -1):
        if thresholds[i] <= maxValue:
            return i + 1
    return 0

def boundConstraintFunctionAssosiatedValueThreshold(dependingValue, assosiatedValues, thresholds):
    maxValue = max([0] + [e.getValue() for e in assosiatedValues])
    #if dependingValue.shortname == "Guter Schütze":
    #    print("Guter Schütze constraint", dependingValue.getValue(), thresholds, maxValue, [e.shortname for e in assosiatedValues])
    if dependingValue.getValue() == 0 or maxValue >= thresholds[dependingValue.getValue() - 1]:
        return -100 # lower bound not set if depending value is 0 or bound is fulfilled by other assosiated value
    return thresholds[dependingValue.getValue() - 1]

class Talent(PurchaseableCharacterPropterty):
    def __init__(self, name, boundUpper, costsFunction, thresholds, assosiatedValues, tooltip):
        PurchaseableCharacterPropterty.__init__(self, "Character", "Talents", name, tooltip, name, 0, boundUpper, 1, costsFunction, None)
        self.boundUpper.add(lambda *d: boundConstraintFunctionDependingValueThreshold(d, thresholds), assosiatedValues, False)
        for e in assosiatedValues: # for assosiated values the lower bound for the bound property of value 0 must not limit assosiated value to values above 0 (see Ini)
            e.boundLower.add(lambda d, a: boundConstraintFunctionAssosiatedValueThreshold(d, a, thresholds), [self, [x for x in assosiatedValues if x != e]], False)
    