class Function:
    def __init__(self, function, arguments):
        self._function = function
        self.arguments = arguments

    def getValue(self):
        return self._function(*self.arguments)

    def getMinimum(self):
        previous = []
        for e in self.arguments:
            previous.append(e.getValue())
            e.setValue(e.boundLower.defaultValue, False)
        minimum = self.getValue()
        for e, p in zip(self.arguments, previous):
            e.setValue(p, False)
        return minimum
    
    def getMaximum(self):
        previous = []
        for e in self.arguments:
            previous.append(e.getValue())
            e.setValue(e.boundUpper.defaultValue, False)
        maximum = self.getValue()
        for e, p in zip(self.arguments, previous):
            e.setValue(p, False)
        return maximum