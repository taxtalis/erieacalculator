from random import randint

def calculatePossibleRolls(diceCount, diceSides, outcomes = None):
    if not outcomes:
        outcomes = [0]
    outcomesNew = []
    for o in outcomes:
        for d in range(1, diceSides+1):
            outcomesNew.append(o+d)
    if diceCount > 1:
        return calculatePossibleRolls(diceCount - 1, diceSides, outcomesNew)
    return outcomesNew

class DiceRoll:
    def __init__(self, diceCount, diceSides):
        self.diceCount = diceCount
        self.diceSides = diceSides
        self.possibleRolls = calculatePossibleRolls(diceCount, diceSides)
        self.rollCount =  len(self.possibleRolls)
        self.minimum = diceCount
        self.maximum = diceCount * diceSides
        self.average = sum(self.possibleRolls) / len(self.possibleRolls)
          
    def roll(self):
        value = 0
        for d in range(self.diceCount):
            value = value + randint(1, self.diceSides)
        return value

DiceRoll_1D4  = DiceRoll(1,  4)
DiceRoll_2D4  = DiceRoll(2,  4)
DiceRoll_1D6  = DiceRoll(1,  6)
DiceRoll_2D6  = DiceRoll(2,  6)
DiceRoll_3D6  = DiceRoll(3,  6)
DiceRoll_2D8  = DiceRoll(2,  8)
DiceRoll_2D10 = DiceRoll(2, 10)
DiceRoll_2D12 = DiceRoll(2, 12)