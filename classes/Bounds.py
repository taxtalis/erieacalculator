class Bound:
    def __init__(self, valueProperty, defaultValue):
        self._valueProperty = valueProperty
        self.defaultValue = defaultValue
        self.boundFunctions = []
        self.boundFunctionsOptimizable = []
    
    def add(self, boundFunction, dependencies, isOptimizable):
        #if self._valueProperty.shortname == "Guter Schütze":
        #    print(self._valueProperty.shortname, "bound to", [d.name for d in dependencies if isinstance(d, ValueProperty)], isOptimizable)
        if isOptimizable:
            self.boundFunctionsOptimizable.append(Function(boundFunction, dependencies))
        else:
            self.boundFunctions.append(Function(boundFunction, dependencies))
            #if self._valueProperty.shortname == "Waffe und Schild":
            #    print("found", [x.shortname for x in dependencies])
            for d in [x for x in dependencies if isinstance(x, ValueProperty)]:
                #if self._valueProperty.shortname == "Waffe und Schild":
                #    print(d.shortname)
                d.dependingProperties.add(self._valueProperty)
        if isinstance(self._valueProperty, ValueProperty):
            self._valueProperty.setValue(self._valueProperty.getValue(), False)
    
class BoundLower(Bound):
    def __init__(self, valueProperty, defaultValue):
        Bound.__init__(self, valueProperty, defaultValue)
    
    def getValue(self):
        #if self._valueProperty.shortname in ["Armbrust"]: 
            #print("Lower", self._valueProperty.shortname, self.defaultValue, [b.getValue() for b in self.boundFunctions])
            #for b in self.boundFunctions:
            #    print([d.shortname for d in b.arguments if isinstance(d, (ValueProperty, Armour))])
        return max([self.defaultValue] + [b.getValue() for b in self.boundFunctions])
    
    def getValueForOptimization(self, unsetValueProperties):
        bound = self.defaultValue
        for b in self.boundFunctionsOptimizable:
            dependencies = set(unsetValueProperties).intersection(b.arguments)
            for e in dependencies:
                    e.setValue(e.boundUpper.getValue(), False)
            bound = max(b.getValue(), bound)
            for e in dependencies:
                    e.setValue(e.boundLower.getValue(), False)
        return bound
    
class BoundUpper(Bound):
    def __init__(self, valueProperty, defaultValue):
        Bound.__init__(self, valueProperty, defaultValue)
    
    def getValue(self):
        #if self._valueProperty.shortname in ["Rüstbeh."]:
           #print("Upper", self._valueProperty.shortname, self.defaultValue, [b.getValue() for b in self.boundFunctions])
        return min([self.defaultValue] + [b.getValue() for b in self.boundFunctions])
    
    def getValueForOptimization(self,unsetValueProperties):
        bound = -1
        for b in self.boundFunctionsOptimizable:
            bound = min(max(b.getValue(), bound), self.defaultValue)

        if bound < 0:
            bound = self.defaultValue

        return bound

from classes.Function import Function
from classes.ValueProperty import ValueProperty
from classes.Armour import Armour