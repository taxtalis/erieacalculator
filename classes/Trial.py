# Level Of Success per diceroll value
LoSTable = [0, 0, 0, # unreachable < 3
            0, 0, 0, # standard slip < 6
            0, 0, 0, 0, 0, # LoS 0: 6-10
            1, 1, 1, # LoS 1: 11-13
            2, 2, 2, # LoS 2: 14-16
            3, 3, 3, # LoS 3: 17-19
            4, 4, 4, # LoS 4: 20-22
            5, 5, 5, # LoS 5: 23-25
            6, 6, 6, # LoS 6: 26-28
            7, 7, 7] # LoS 7: 29-31

class Trial:
    def __init__(self, characterProperty):
        self.characterProperty = characterProperty
        self.boundLower = BoundLower(self, 6)
        self.diceRoll = DiceRoll_3D6
        self.lastValue = 0
        
    def chances(self, adjustment = 0):
        #print("Calculating chances for", self.characterProperty.shortname, "with adjustment", adjustment)
        propertyValue = self.characterProperty.getValue()
        boundLower = self.boundLower.getValue()
        LosList = [LoSTable[diceRoll + propertyValue + adjustment] for diceRoll in self.diceRoll.possibleRolls if diceRoll >= boundLower]
        chances = {i:LosList.count(i) for i in LosList}
        for LoS in range(8):
            chances[LoS] = chances.get(LoS, 0) / self.diceRoll.rollCount
        chances["Slip"] = len([diceRoll for diceRoll in self.diceRoll.possibleRolls if diceRoll < boundLower]) / self.diceRoll.rollCount
        return chances
        
    def roll(self, adjustment = 0):
        propertyValue = self.characterProperty.getValue()
        boundLower = self.boundLower.getValue()
        LoS = 0
        slip = True
        self.lastValue = self.diceRoll.roll()
        if self.lastValue >= boundLower:
            self.lastValue = self.lastValue + propertyValue + adjustment
            LoS = LoSTable[self.lastValue]
            slip = False

        return LoS, slip, self.characterProperty.name
    
    def adjustLastRoll(self, adjustment = 0):
        boundLower = self.boundLower.getValue()
        LoS = 0
        slip = True
        if self.lastValue >= boundLower:
            LoS = LoSTable[self.lastValue + adjustment]
            slip = False
        return LoS, slip, self.characterProperty.name
        
    
class MultiTrial:
    def __init__(self, trials):
        self.trials = trials
        
    def chances(self, adjustments = None):
        if not adjustments:
            adjustments = [0]*len(self.trials)
        trialChances = [trial.chances(adjustment) for trial, adjustment in zip(self.trials, adjustments)]
        trialChancesProduct = list(product(*trialChances))
        chances = {}
        for trialChanceKeys in trialChancesProduct:
            LoS = "Slip"
            trialChance = 1
            for k, tc in zip(trialChanceKeys, trialChances):
                trialChance = trialChance * tc[k]
            if "Slip" not in trialChanceKeys:
                LoS = max(trialChanceKeys)
            chances[LoS] = chances.get(LoS, 0) + trialChance
        return chances
        
    def roll(self, adjustments = None):
        if not adjustments:
            adjustments = [0]*len(self.trials)
        outcome = []
        for trial, adjustment in zip(self.trials, adjustments):
            outcome.append(trial.roll(adjustment))
        return outcome
    
from classes.Bounds import BoundLower
from classes.DiceRoll import DiceRoll_3D6
from itertools import product