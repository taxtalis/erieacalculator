import PySimpleGUI as psg
from classes.Trial import MultiTrial
from helperFunctions import round_halfup
from classes.DiceRoll import DiceRoll_1D6

def calculateAverageDamage(weapons, chances):
    damage = 0
    for w in weapons:
        _, avg, _ = w.damageStats()
        damage = damage + avg
    damage = damage / len(weapons)
    bonus = DiceRoll_1D6.average
    IPCostsForAttackCycle = sum([x.IPCostsForAttackCycle.calculate() for x in weapons]) / len(weapons)
    return (damage + sum([chances[key] * key * bonus for key in chances if key != "Slip"])) / IPCostsForAttackCycle

class BaseUI:
    def __init__(self, category, objectID, name, tooltip = None):
        self.category = category
        self.objectID = objectID
        self.name = name
        self.tooltip = tooltip
        self.elements = []
        
    def generateKey(self):
        return "#".join(["ValueProperty", self.category, str(self.objectID), str(len(self.elements))])

class UIDamage(BaseUI):
    def __init__(self, damageFunction, category, uuid, name, tooltip = None):
        BaseUI.__init__(self, category, uuid, tooltip)
        self.damageFunction = damageFunction
        
    def update(self, updated=None):
        value = self.getValue()
        for ui in self.elements:
            ui.update(value)
     
    def getValue(self):
        minimum, average, maximum = self.damageFunction()
        return str(minimum) + " < " + str(average) + " < " + str(maximum)
     
    def addElement(self, elementType, text = None):
        if not text:
            text = self.name
            
        if elementType == "Text":
            ui = psg.Text(text=self.getValue(), 
                          tooltip=self.tooltip)
        self.elements.append(ui)
        return ui
    
class UIEquippedWeaponStats(BaseUI):
    def __init__(self, weaponsMH, weaponsOH, category, uuid, name, tooltip = None):
        BaseUI.__init__(self, category, uuid, tooltip)
        self.weaponsMH = weaponsMH
        self.weaponsOH = weaponsOH
        
    def update(self, updated=None):
        headings, values = self.getContent()
        for ui in self.elements:
            ui.update(values = values)

    def getContent(self):
        weapons = []
        isMainhand = False
        isOffhand = False
        for w in self.weaponsMH:
            if w.getValue():
                isMainhand = True
                weapons.append(w)
                break
        for w in self.weaponsOH:
            if w.getValue():
                isOffhand = True
                weapons.append(w)
                break
        
        trial = MultiTrial(weapons)
        chances = {}
        headings = ["Patzer", "0", "1", "2", "3", "4", "5", "6", "7", "~S/IP"]
        values = []
        damage = 0
        if weapons != [] and (not isOffhand or isMainhand):
            chances = trial.chances()
            damage = calculateAverageDamage(weapons, chances)
        for h in ["Slip", 0, 1, 2, 3, 4, 5, 6, 7]:
            values.append(round(round_halfup(chances.get(h, 0)*10000)/100, 2))
        values.append(round(round_halfup(damage*100)/100, 2))
        return headings, [values]

    def addElement(self, elementType, text = None):
        headlines, values = self.getContent()
        ui = psg.Table(values, headlines, def_col_width=5, auto_size_columns=False, hide_vertical_scroll=True, justification="center", num_rows=1)
        self.elements.append(ui)    
        return ui
    
class UI(BaseUI):
    def __init__(self, characterProperty, category, sliderResolution, tooltip = None):
        BaseUI.__init__(self, category, characterProperty.uuid, characterProperty.name, tooltip)
        self.characterProperty = characterProperty
        self.sliderResolution = sliderResolution
    
    def update(self, updated=None):
        if not updated:
            updated = set([self.characterProperty])
        value = self.characterProperty.getValue()
        for ui in self.elements:
            ui.update(value)
        dependingProperties = self.characterProperty.dependingProperties.difference(updated)
        updated.update(dependingProperties)
        #print("Update", self.characterProperty.shortname, 
        #      #[e.shortname for e in updated], 
        #      [e.shortname for e in dependingProperties])
        for e in dependingProperties:
            e.UI.updateRanges()
            e.update(updated)

        # necessary for derived Properties to set their ghost value for optimization process
        self.characterProperty.value = value 
            
    def addElement(self, elementType, text = None):
        if not text:
            text = self.name
        
        if elementType == "Label":
            return psg.Text(text)
        elif elementType == "Text":
            value = self.characterProperty.getValue()
            ui = psg.Text(text=value, 
                          key=self.generateKey(), 
                          tooltip=self.tooltip)
            
        elif elementType == "Slider":
            lower = self.characterProperty.boundLower.getValue()
            upper = self.characterProperty.boundUpper.getValue()
            value = self.characterProperty.getValue()
            ui = psg.Slider(range=(lower, upper), 
                            resolution=self.sliderResolution, 
                            default_value=value, 
                            expand_x=True, 
                            enable_events=True, 
                            orientation='horizontal', 
                            key=self.generateKey(),
                            tooltip=self.tooltip)
        else:
            value = self.characterProperty.getValue()
            ui = psg.Checkbox(text=text,
                              default=False, 
                              key=self.generateKey(), 
                              enable_events=True, 
                              tooltip=self.tooltip)
        
        self.elements.append(ui)
        return ui

    def updateRanges(self):
        lower = self.characterProperty.boundLower.getValue()
        upper = self.characterProperty.boundUpper.getValue()
        #if self.characterProperty.shortname == "Armbrust":
        #    print("Armbrust bound update", lower, upper)
        for e in self.elements:
            if type(e) == psg.Slider:
                e.update(range=(lower, upper))