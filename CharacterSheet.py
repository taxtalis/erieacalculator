from classes.Character import Character
from helperFunctions import round_halfup

import PySimpleGUI as psg
from optimizerAttributes import optimizeAttributes
from optimizerArmour import optimizeArmour
from optimizerWeapon import optimizeWeapons

character = Character()

def generateWindowDerivedProperties():
    layout_derivedProperties_attributes = []
    for e, d in [[character.ST, character.gGST], 
                 [character.KO, character.gGKO],
                 [character.GE, character.gGGE],
                 [character.IN, character.gGIN],
                 [character.WI, character.gGWI],
                 [character.MU, character.gGMU],
                 [character.WA, character.gGWA]]:
        label = psg.Text(e.name)
        text = e.UI.addElement("Text")
        blessed = [d.UI.addElement("Checkbox", "Gesegnet")]
        layout_derivedProperties_attributes.append([label, text])
        layout_derivedProperties_attributes.append(blessed)
        layout_derivedProperties_attributes.append([psg.HorizontalSeparator()])
    del layout_derivedProperties_attributes[-1]
    
    layout_derivedProperties_derivedProperties = []
    for e, d in [[character.Ref, character.gRef], 
                 [character.Res, character.gRes],
                 [character.Aus, character.gAus],
                 [character.Bel, character.gBel],
                 [character.Ini, character.gIni],
                 [character.LP, [character.sLP, character.gLP]],
                 [character.FP, character.gFP]]:
        label = psg.Text(e.name, (10, 0))
        slider = e.UI.addElement("Slider")
        layout_derivedProperties_derivedProperties.append([label, slider])
        if(isinstance(d, list)):
            advantage1 = d[0].UI.addElement("Checkbox")
            advantage2 = d[1].UI.addElement("Checkbox")
            layout_derivedProperties_derivedProperties.append([advantage1, advantage2])
        elif(d):
            advantage = d.UI.addElement("Checkbox")
            layout_derivedProperties_derivedProperties.append([advantage])
        layout_derivedProperties_derivedProperties.append([psg.HorizontalSeparator()])
    del layout_derivedProperties_derivedProperties[-1]
    
    layout_derivedProperties = [[psg.Button("Attribute Optimieren", key="OptimizeAttributes"), 
                                 psg.Checkbox("Verhindere Attribut-Verminderung", key="OptimizeKeepAttributes")], 
                        [psg.Column(layout_derivedProperties_attributes), psg.Column(layout_derivedProperties_derivedProperties)]]
    window_derivedProperties = psg.Window('Attribute Optimieren', layout_derivedProperties, finalize=True)
    
    return window_derivedProperties
    
def generateCharacterPropertyUIEntityList(name, showXP, sorting, properties, elementType):
    layout_column = [[psg.Text(name, pad=((40, 0), (20, 0)))]]
    if showXP:
        xp =  character.createUIXPEntity(name)
        xp.Pad = ((40, 0), (20, 0))
        layout_column[0].append(xp)
    if sorting:
        properties = sorted(properties, key=lambda e: e.name)
    for e in properties:
        if elementType == "Slider":
            label = psg.Text(e.name, (20, 1))
            entity = e.UI.addElement(elementType)
            layout_column.append([label, entity])
        else:
            entity = e.UI.addElement(elementType)
            layout_column.append([entity])
    return layout_column

def generateWindowSkills():
    layout_skills =[[psg.Text("Fertigkeiten"), character.createUIXPEntity("Fertigkeiten")]]
    skillsColumns = []
    skillsColumn = []
    skillsColumn.extend(generateCharacterPropertyUIEntityList("Körperliche Fertigkeiten", True, True, character.skillsPhysical, "Slider"))
    skillsColumn.append([psg.HorizontalSeparator()])
    skillsColumn.extend(generateCharacterPropertyUIEntityList("Erfahrungsfertigkeiten", True, True, character.skillsPractical, "Slider"))
    skillsColumns.append(psg.Column(skillsColumn, vertical_alignment='t'))
    
    skillsColumn = []
    skillsColumn.extend(generateCharacterPropertyUIEntityList("Rhetorische Fertigkeiten", True, True, character.skillsRethorical, "Slider"))
    skillsColumn.append([psg.HorizontalSeparator()])
    skillsColumn.extend(generateCharacterPropertyUIEntityList("Kampffertigkeiten", True, True, character.skillsCombat, "Slider"))
    skillsColumns.append(psg.Column(skillsColumn, vertical_alignment='t'))
    
    skillsColumn = []
    skillsColumn.extend(generateCharacterPropertyUIEntityList("Wissensfertigkeiten", True, True, character.skillsKnowledge, "Slider"))
    skillsColumns.append(psg.Column(skillsColumn, vertical_alignment='t'))
    
    skillsColumn = []
    skillsColumn.extend(generateCharacterPropertyUIEntityList("Vergessene Kunst", True, True, character.skillsAncientArts, "Slider"))
    skillsColumn.append([psg.HorizontalSeparator()])
    skillsColumn.extend(generateCharacterPropertyUIEntityList("Lehren der fünf Schöpfer", True, True, character.skillsWondersPantheon, "Slider"))
    skillsColumns.append(psg.Column(skillsColumn, vertical_alignment='t'))
    
    skillsColumn = []
    skillsColumn.extend(generateCharacterPropertyUIEntityList("Sprachen", True, True, character.skillsLanguage, "Slider"))
    skillsColumn.append([psg.HorizontalSeparator()])
    skillsColumn.extend(generateCharacterPropertyUIEntityList("Schriften", True, True, character.skillsWriting, "Slider"))
    skillsColumns.append(psg.Column(skillsColumn, vertical_alignment='t'))
    
    layout_skills.append(skillsColumns)
    window = psg.Window("Fertigkeiten", [layout_skills], finalize=True)
    return window

def generateWindowDisAdvantages():
    layout_advantages = generateCharacterPropertyUIEntityList("Gaben", True, True, character.advantages, "Checkbox")
    layout_disadvantages = generateCharacterPropertyUIEntityList("Schwächen", True, True, character.disadvantages, "Checkbox")
    window = psg.Window("Gaben und Schwächen", [[psg.Column(layout_advantages, vertical_alignment='t'), psg.Column(layout_disadvantages, vertical_alignment='t')]], finalize=True)
    return window

def generateWindowTalents():
    layout_talents = [[psg.Text("Besondere Talente"), character.createUIXPEntity("Besondere Talente")]]
    talentsColumns = []
    talentsColumn = []
    talentsColumn.extend(generateCharacterPropertyUIEntityList("Waffentalente", True, True, character.talentsWeapons, "Slider"))
    talentsColumns.append(psg.Column(talentsColumn, vertical_alignment='t'))
    talentsColumn = []
    talentsColumn.extend(generateCharacterPropertyUIEntityList("Arkana", True, True, character.talentsAncientArt, "Slider"))
    talentsColumn.append([psg.HorizontalSeparator()])
    talentsColumn.extend(generateCharacterPropertyUIEntityList("Wunder", True, True, character.talentsWonders, "Slider"))
    talentsColumns.append(psg.Column(talentsColumn, vertical_alignment='t'))
    
    layout_talents.append(talentsColumns)
    window = psg.Window("Besondere Talente", [layout_talents], finalize=True)
    return window

def generateWindowArmour():
    layout_armour_statrow = []
    for e in [character.equipment.Beh] + character.equipment.armourRatings:
        layout_armour_statrow.extend([psg.Text(e.name), e.UI.addElement("Text")])
    
    layout_armour_columns = []
    layout_armour_column = []
    layout_armour_column.extend(generateCharacterPropertyUIEntityList("Voll", False, False, character.equipment.armoursFull, "Checkbox"))
    layout_armour_columns.append(psg.Column(layout_armour_column, vertical_alignment='t'))
    layout_armour_column = []
    layout_armour_column.extend(generateCharacterPropertyUIEntityList("Kopf", False, False, character.equipment.armoursHead, "Checkbox"))
    layout_armour_columns.append(psg.Column(layout_armour_column, vertical_alignment='t'))
    layout_armour_column = []
    layout_armour_column.extend(generateCharacterPropertyUIEntityList("Torso", False, False, character.equipment.armoursChest, "Checkbox"))
    layout_armour_columns.append(psg.Column(layout_armour_column, vertical_alignment='t'))
    layout_armour_column = []
    layout_armour_column.extend(generateCharacterPropertyUIEntityList("Arme", False, False, character.equipment.armoursArms, "Checkbox"))
    layout_armour_columns.append(psg.Column(layout_armour_column, vertical_alignment='t'))
    layout_armour_column = []
    layout_armour_column.extend(generateCharacterPropertyUIEntityList("Beine", False, False, character.equipment.armoursLegs, "Checkbox"))
    layout_armour_columns.append(psg.Column(layout_armour_column, vertical_alignment='t'))
        
    layout_armour_column = []
    label = psg.Text("Erlaubter Ini-Malus")
    slider = psg.Slider(range=(0, 3), resolution=1, default_value=0, 
                        expand_x=True, enable_events=False, 
                        orientation='vertical', key="OptimizeArmourIniMalusAllowance")
    
    layout_armour_column.append([label, slider])
    layout_armour_column.append([psg.Checkbox("Erlaube Belasted", key="OptimizeArmourAllowEncumbered")])
    layout_armour_column.append([psg.Checkbox("Bevorzuge Rüstungsvollschutz", key="OptimizeArmourPreferFull")])
    layout_armour_column.append([psg.Checkbox("Optimiere für Trefferzonen", key="OptimizeArmourHitzones")])
    layout_armour_column.append([psg.Button("Rüstungsschutz Optimieren", key="OptimizeArmour")])
    layout_armour_columns.append(psg.Column(layout_armour_column, vertical_alignment='t'))
    
    window = psg.Window("Rüstung", [layout_armour_statrow, layout_armour_columns], finalize=True)
    return window

def generateWindowTrials():
    layout_trials = [psg.Button("Zurücksetzen", key="TrialReset"), psg.Button("Probe Würfeln", key="TrialRoll")]
    layout_trials_column = []
    for e in character.attributes + [character.Ref, character.Res, character.Aus] + character.skills:
        layout_trials_column.append([psg.Checkbox(e.name, key="Trial#"+ e.uuid)])
    window = psg.Window("Probe", [layout_trials, [psg.Column(layout_trials_column, scrollable=True, vertical_scroll_only=True)]], finalize=True, size=(300, 800))
    return window

def generateWindowWeapons():   
    layout_main = []
    layout_main.append(character.equipment.uiWeapons.addElement("Table"))
    layout_main.append(psg.Button("Kampfwurf", key="TrialCombat"))
    layout_main.append(psg.Button("Waffen Optimieren", key="OptimizeWeapons"))
    layout_weapons_meele = [[psg.Checkbox("Optimiere für Nahkampfwaffen", key="OptimizeMeele", default=True)]]
    layout_weapons_range = [[psg.Checkbox("Optimiere für Fernkampfwaffen", key="OptimizeRanged", default=True)]]
    layout_weapons_shield = [[psg.Checkbox("Optimiere für Shilde", key="OptimizeShield", default=True)]]
    layout_weapons_shield.extend(generateCharacterPropertyUIEntityList("Schilde", False, False, character.equipment.shields, "Checkbox"))
    for w in character.equipment.weaponsMH:
        layout_weapon = []
        layout_weapon.append(w.UI.addElement("Label"))
        layout_weapon.append(w.UI.addElement("Checkbox", "MH"))
        for o in [o for o in w.copies if o.isOffhand]:
            layout_weapon.append(o.UI.addElement("Checkbox", "OH"))
        layout_weapon.append(psg.Text("Schaden:"))
        layout_weapon.append(w.UIDamage.addElement("Text"))
        
        if w.reach < 3:
            layout_weapons_meele.append(layout_weapon)
        else:
            layout_weapons_range.append(layout_weapon)
    window = psg.Window("Waffen und Schilde", [layout_main, 
                                   [psg.Column(layout_weapons_meele, vertical_alignment='t'),
                                    psg.Column(layout_weapons_range, vertical_alignment='t'),
                                    psg.Column(layout_weapons_shield, vertical_alignment='t')]], finalize=True)
    return window

row_menu = []
row_menu.append(psg.Button("Attribute Optimieren", key="DerivedProperties"))
row_menu.append(psg.Button("Fertigkeiten", key="Skills"))
row_menu.append(psg.Button("Gaben und Schwächen", key="DisAdvantages"))
row_menu.append(psg.Button("Besondere Talente", key="Talents"))
row_menu.append(psg.Button("Rüstung", key="Armour"))
row_menu.append(psg.Button("Waffen", key="Weapons"))
row_menu.append(psg.Button("Probe", key="Trials"))
row_menu.append(psg.Text("Gesamt EP: "))
row_menu.append(character.createUIXPEntity(None))
layout_main = [row_menu]

layout_main_attributes = [[psg.Text("Attribute EP: "), character.createUIXPEntity("Attribute")]]
for e in character.attributes:
    label = psg.Text(e.name, (15, 1))
    slider = e.UI.addElement("Slider")
    layout_main_attributes.append([label, slider])

layout_main_derivedProperties = []
for e in character.derivedProperties:
    label = psg.Text(e.name, (10, 1))
    text = e.UI.addElement("Text")
    layout_main_derivedProperties.append([label, text])

layout_main.append([[psg.Column(layout_main_attributes), psg.Column(layout_main_derivedProperties)]])

windows = {"DerivedProperties": generateWindowDerivedProperties(),
           "Skills": generateWindowSkills(),
           "DisAdvantages": generateWindowDisAdvantages(),
           "Talents": generateWindowTalents(),
           "Armour": generateWindowArmour(),
           "Weapons": generateWindowWeapons(),
           "Trials": generateWindowTrials()}

for window in windows.values():
    window.hide()

window_main = psg.Window('ERIEA Charakter Rechner', layout_main, finalize=True)
windows["Main"] = window_main

while True:
    window, event, values = psg.read_all_windows()

    if event == psg.WIN_CLOSED or event == 'Exit':
        window.Hide()
        if window == window_main:
            break
    else:
        # buttons to unhide windows
        if event in windows:
            windows[event].UnHide()
        # propagate any value change for a character property
        elif event.startswith("ValueProperty"):
            prefix, category, uuid, index = event.split("#")
            character.uiEvent(category, uuid, values[event])
        elif event == "OptimizeAttributes":
            keepAttributes = values["OptimizeKeepAttributes"]
            optimizeAttributes(character, keepAttributes)
        elif event == "OptimizeArmour":
            optimizeArmour(character, values["OptimizeArmourIniMalusAllowance"], values["OptimizeArmourAllowEncumbered"], values["OptimizeArmourPreferFull"], values["OptimizeArmourHitzones"])
        elif event == "TrialRoll":
            uuids = []
            for k, v in values.items():
                if k.startswith("Trial#") and v == True:
                    prefix, uuid = k.split("#")
                    uuids.append(uuid)
            if uuids != []:
                outcome = character.trial(uuids)
                if any([o[1] for o in outcome]):
                    psg.popup_ok("Patzer in Probe auf", *[o[2] for o in outcome if o[1]], title="Patzer!", text_color="red")
                else:
                    psg.popup_ok("Probe(n) erreichte ein Erfolgsniveaupool von", sum([o[0] for o in outcome]), title="Erfolg!")
        elif event == "TrialReset":
            for ui in windows["Trials"].element_list():
                if type(ui) == psg.Checkbox:
                    ui.update(0)
        elif event == "TrialCombat":
            outcome, damages = character.equipment.trialCombat()
            if outcome != []:
                if any([o[1] for o in outcome]):
                    psg.popup_ok("Patzer in Probe auf", *[o[2] for o in outcome if o[1]], title="Patzer!", text_color="red")
                else:
                    psg.popup_ok("Probe(n) erreichte(n) ein Erfolgsniveau von", *[o[2] + ": " + str(o[0]) + ", Basisschaden: " + str(d) for o, d in zip(outcome, damages)], title="Erfolg!")
        elif event == "OptimizeWeapons":
            allowMeele = values["OptimizeMeele"]
            allowRanged = values["OptimizeRanged"]
            allowShield = values["OptimizeShield"]
            optimizeWeapons(character, allowMeele, allowRanged, allowShield)
            
for window in windows.values():
    window.close()