def round_halfup(value):
    if(value < 0):
        value = int(value - 0.5)
    else:
        value = int(value + 0.5)
    return value
