from classes.Character import Character

def variance(values):
    # Calculate mean
    mean = sum(values) / len(values)
    
    # Calculate squared differences from the mean
    squared_diff = [(x - mean) ** 2 for x in values]
    
    # Calculate variance (mean of squared differences)
    return sum(squared_diff) / len(squared_diff)

iBeh = 0
iRS = iBeh + 1
iRSV = iRS + 1
iVar = iRSV + 1
iEquip = iVar + 1

def optimizeArmourBodyPart(character, solution, armoursByType, iniMalusAllowance, allowEncumbered, preferFull, optimizeAllHitZones):
    for a in armoursByType[0]:
        if a.iniMalus <= iniMalusAllowance:
            a.setValue(1, False)
            if len(armoursByType) > 1:
                optimizeArmourBodyPart(character, solution, 
                                       armoursByType[1:], 
                                       iniMalusAllowance, allowEncumbered, preferFull, optimizeAllHitZones)
            else:
                Beh = character.equipment.Beh.getValue()
                BehA = character.equipment.BehA.getValue()
                RS = character.equipment.RS.getValue(False)
                RST = character.equipment.RST.getValue()
                RSA = character.equipment.RSA.getValue()
                RSB = character.equipment.RSB.getValue()
                RSV = character.equipment.RSV.getValue()
                Var = variance([RSA, RST, RSB]) # helmet is only depending on iniMalus and irrelevant here
                
                overwrite = False
                # character can either wear the set without being encumbered or the set itself is not adding encumbrance
                if ((Beh <= 0 + 2 * allowEncumbered or BehA <= 0)
                    # discard sets where parts deviate too much from each other if hitZone optimization enabled (threshold chosen arbitrarily)
                    and (not optimizeAllHitZones or Var < 1.5)):
                    if solution != []:
                        # prefer sets with higher RS with bias towards higher RSV (factor chosen arbitrarily), increase bias when RSV is preferred
                        if solution[iRS] + solution[iRSV] * (0.5 + preferFull) < RS + RSV * (0.5 + preferFull):
                            overwrite = True
                            # should biased RS be equl choose set with less diviation in its parts
                        elif solution[iRS] + solution[iRSV] * (0.5 + preferFull) == RS + RSV * (0.5 + preferFull) and solution[iVar] > Var:
                            overwrite = True
                        
                    else:
                        overwrite = True
                if overwrite:
                    solution.clear()
                    solution.append(Beh)
                    solution.append(RS)
                    solution.append(RSV)
                    solution.append(Var)
                    solution.append(character.equipment.save())

            a.setValue(0, False)
        
def optimizeArmour(character, iniMalusAllowance, allowEncumbered, preferFull, optimizeAllHitZones):
    
    equipment = character.equipment
    
    for a in equipment.armours:
        a.setValue(0, False)
                    
    solution = []
    optimizeArmourBodyPart(character, solution, 
                           [equipment.armoursFull], 
                           iniMalusAllowance, allowEncumbered, preferFull, optimizeAllHitZones)
    optimizeArmourBodyPart(character, solution, 
                           [equipment.armoursHead, equipment.armoursChest, equipment.armoursArms, equipment.armoursLegs], 
                           iniMalusAllowance, allowEncumbered, preferFull, optimizeAllHitZones)

    
    character.equipment.load(solution[iEquip])
    #character.uiRefresh()

def test(Bel, iniMalusAllowance, allowEncumbered, output):
    character = Character()
    character.KO.setValue(Bel, False)
    character.ST.setValue(Bel-10, False)
    character.gBel.setValue(Bel==22, False)
    optimizeArmour(character, iniMalusAllowance, allowEncumbered)
    if output:
        print("Bel", character.equipment.Beh.getValue(), "RS", character.equipment.RS.getValue())
        for a in character.equipment.armours:
            if a.value == 1:
                print(a.armourType, a.name, a.armourRating, a.armourRatingFactored(), a.encumbrance, a.iniMalus)
    
#test(14, 3, False, True)