from classes.CharacterProperty import Attribute, Advantage, Disadvantage
from classes.Character import Character


def checkForSolution(character, ST, KO, GE, IN, WI, MU, WA):
    if ST and character.ST.getValue() != ST:
        return False
    if KO and character.KO.getValue() != KO:
        return False
    if GE and character.GE.getValue() != GE:
        return False
    if IN and character.IN.getValue() != IN:
        return False
    if WI and character.WI.getValue() != WI:
        return False
    if MU and character.MU.getValue() != MU:
        return False
    if WA and character.WA.getValue() != WA:
        return False
    return True

def optimizeBonuses(character, derivedProperty):
            
    valid = False
    difference = derivedProperty.getValue() - derivedProperty.value
    bonusMaximum = derivedProperty.derivation.bonusOptimizableMaximum 
    malusMaximum = derivedProperty.derivation.malusOptimizableMaximum

    if bonusMaximum > 0 and difference < 0 and difference + bonusMaximum >= 0:
        for d in derivedProperty.derivation.getBonusDependencies(False, True):
            d.setValue(d.boundUpper.getValue(), False)
            if derivedProperty.value - derivedProperty.derivation.calculate() <= 0:
                valid = True
                break
    elif malusMaximum > 0 and difference > 0 and difference - malusMaximum >= 0:
        for d in derivedProperty.derivation.getMalusDependencies(False, True):
            d.setValue(d.boundUpper.getValue(), False)
            if derivedProperty.value - derivedProperty.derivation.calculate() <= 0:
                valid = True
                break
    else:
        valid = True
        
    return valid

def optimizeAttribute(character, unsetAttributes, lowerBounds, depth, solution):
    e = unsetAttributes[0]
    unsetAttributesNew = unsetAttributes[1:]
    lower = e.boundLower.getValueForOptimization(unsetAttributes)
    upper = e.boundUpper.getValueForOptimization(unsetAttributes)
    for value in range(max(lower, lowerBounds[0]), upper + 1):
        e.setValue(value, False)
        #print(e.shortname, lower, value, upper)
        valid = True
        satisfiedDependencies = []
        for d in set(character.derivedProperties).intersection(e.dependingProperties):
            if len(set(d.derivation.getBaseDependencies()).intersection(unsetAttributesNew)) == 0:
                satisfiedDependencies.append(d)
                if not optimizeBonuses(character, d):
                    valid = False
                    break
        if valid:
            if depth > 1:
                optimizeAttribute(character, unsetAttributesNew, lowerBounds[1:], depth - 1, solution)
            else:
                cost = character.costs(None)      
                if solution == [] or solution[0] > cost or (solution[0] == cost and max(solution[1][:8]) < max([e.value for e in character.attributes])):
                    solution.clear()
                    solution.append(cost)
                    solution.append(character.save())
                    
                                
        for d in satisfiedDependencies:
            #if checkForSolution(character, 0, 8, 0, 1, 1, 0, 0) and e.shortname == "KO":
            #    print("unset", [s.shortname for s in d.derivation.getBonusDependencies(False, True).union(d.derivation.getMalusDependencies(False, True))])
            for b in d.derivation.getBonusDependencies(False, True).union(d.derivation.getMalusDependencies(False, True)):
                b.setValue(b.boundLower.getValue(), False)
    e.setValue(e.boundLower.getValue(), False)
     



def resetProperties(properties):
    for e in properties:
        e.setValue(e.boundLower.getValue(), False)

def saveProperties(properties):
    values = []
    for e in properties:
        values.append(e.getValue())
    return values

def loadProperties(properties, values, updateUI):
    for e, v in zip(properties, values):
        e.setValue(v, updateUI)
    

def optimizeAttributes(character, useCurrentAttributes):   
    optimizableProperties = []
    optimizableProperties.extend(character.attributes)
    optimizableProperties.remove(character.WI) # explicitely set anyway
    optimizableProperties.remove(character.CH) # does not play any role in process and can be left untouched
    optimizableProperties.extend(character.advantagesDerivedProperties)
    optimizableProperties.extend(character.disadvantagesDerivedProperties)
    optimizableProperties.extend(character.advantagesBlessed)
    
    unOptimizableProperties = [x for x in character.properties if x not in optimizableProperties and x not in character.attributes]
    
    preOptimizationValues = saveProperties(unOptimizableProperties)
    
    lowerBoundWI = character.WI.getValue()
    
    boundsLowerA = [0]
    boundsLowerB = [0, 0]
    boundsLowerC = [0, 0, 0]
    if useCurrentAttributes:
        boundsLowerA[0] = character.IN.getValue()
        
        boundsLowerB[0] = character.KO.getValue()
        boundsLowerB[1] = character.ST.getValue()
        
        boundsLowerC[0] = character.WA.getValue()
        boundsLowerC[1] = character.MU.getValue()
        boundsLowerC[2] = character.GE.getValue()
    
    # find solution
    solution = None
    
    e = character.WI
    unsetAttributes = [character.IN, character.KO, character.ST, character.WA, character.MU, character.GE]
    lower = e.boundLower.getValueForOptimization(unsetAttributes)
    upper = e.boundUpper.getValueForOptimization(unsetAttributes)

    for value in range(max(lower, lowerBoundWI), upper + 1):
        #print(e.shortname, lower, value, upper)
        e.setValue(value, False)
        
        # reset optimizable parameters
        resetProperties(optimizableProperties)
        
        unsetAttributes = [character.IN, character.KO, character.ST, character.WA, character.MU, character.GE]
        solutionA = []
        optimizeAttribute(character, unsetAttributes, boundsLowerA, 1, solutionA)
        
        if solutionA != []:
            unsetAttributes = [character.KO, character.ST, character.WA, character.MU, character.GE]
            character.load(solutionA[1], False)
            solutionB = []

            optimizeAttribute(character, unsetAttributes, boundsLowerB, 2, solutionB)
                
            if solutionB != []:
                unsetAttributes = [character.WA, character.MU, character.GE]
                character.load(solutionB[1], False)
                solutionC = []
                
                optimizeAttribute(character, unsetAttributes, boundsLowerC, 3, solutionC)
                #print(solutionC[0], solutionC[1][:8])
                if solutionC != []:
                    if not solution or solution[0] > solutionC[0] or (solution[0] == solutionC[0] and max(solution[1][:8]) < max(solutionC[1][:8])):
                        solution = solutionC
    # set best solution
    character.load(solution[1], True)
    # restore all un-optimizable values
    loadProperties(unOptimizableProperties, preOptimizationValues, True)
        
    # set blessed for highest set attribute if attribute >= 8
    # costs(8) - costs(7) > 40, therefor 8 is the first value blessed will be cheaper
    highestAttribute = None
    for e in character.attributes:
        if not highestAttribute or highestAttribute.value < e.value:
            highestAttribute = e
    if highestAttribute.value >= 8:
       set(highestAttribute.costs.dependencies).intersection(character.advantagesBlessed).pop().setValue(1, True)
        
    #character.uiRefresh()
    character.uiUpdateXP()

         
def test(output, ref, res, aus, bel, ini, lp, fp):
    character = Character()
    
    #char.armour.sIni.setValue(3, False)
    
    character.Ref.setValue(ref, False)
    character.Res.setValue(res, False)
    character.Aus.setValue(aus, False)
    character.Bel.setValue(bel, False)
    character.Ini.setValue(ini, False)
    character.LP.setValue(lp, False)
    character.FP.setValue(fp, False)
    optimizeAttributes(character)
    
    if output:
        print("Cost: ", character.costs(None))
        for e in character.properties:
            if isinstance(e, (Attribute, Advantage, Disadvantage)):
                print(e.value, e.shortname)
    
    return character.costs(None)

#test(True, 6, 1, 7, 1, 15, 55, 4)
#test(True, 10, 4, 0, 0, 0, 0, 0)
#test(True, 10, 0, 6, 11, 8, 60, 3)
#test(8, 0, 10, 15, 14, 15, 15)
#test(10, 7, 0, 0, 19, 19, 14)
#test(3, 3, 3, 3, 3, 10, 3)
#test(5, 5, 5, 11, 11, 35, 7)
#test(3, 3, 10, 22, 22, 70, 15)
#test(0, 0, 0, 0, 0, 15, 0)